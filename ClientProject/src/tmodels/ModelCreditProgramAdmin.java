﻿package tmodels;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import vo.*;

public class ModelCreditProgramAdmin implements TableModel {
	private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
	private static List<CreditProgram> creditPrograms;
	private static Map<Integer, CreditProgram> creditProgramsMap = new HashMap<>();
	
	
	
	public ModelCreditProgramAdmin(List<CreditProgram> creditPrograms) {
		this.creditPrograms = creditPrograms;
		
		
	}

	@Override
	public int getRowCount() {
		return creditPrograms.size();
	}

	@Override
	public int getColumnCount() {
		 return 8;
	}

	@Override
	public String getColumnName(int columnIndex) {
		 switch (columnIndex) {
	        case 0:
	            return "№";
	        case 1:
	            return "Назва";
	        case 2:
	            return "Короткий опис";
	        case 3:
	            return "Період(міс.)";
	        case 4:
	            return "Макс. сума(грн.)";
	        case 5:
	            return "Miн. сума(грн.)";
	        case 6:
	            return "Процент(%)";
	        case 7:
	            return "Статус";
	        }
	    return "";
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		 switch (columnIndex) {
	        case 0:
	            return Integer.class;
	        case 1:
	            return String.class;
	        case 2:
	            return String.class;
	        case 3:
	            return Integer.class;
	        case 4:
	            return Integer.class;
	        case 5:
	            return Integer.class;
	        case 6:
	            return Integer.class;
	        case 7:
	            return String.class;
	        }
	    return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		CreditProgram creditProgram = creditPrograms.get(rowIndex);
		creditProgramsMap.put(creditProgram.getIdCreditProgram(), creditProgram); 
	    switch (columnIndex) {
	        case 0:
	            return creditProgram.getIdCreditProgram();
	        case 1:
	            return creditProgram.getName();
	        case 2:
	            return creditProgram.getShortDescription();
	        case 3:
	            return creditProgram.getMaxPeriod();
	        case 4:
	            return creditProgram.getMaxMoneyAmount();
	        case 5:
	            return creditProgram.getMinMoneyAmoun();
	        case 6:
	            return creditProgram.getPercent()*100;
	        case 7:
	            return "unblocked".equals(creditProgram.getStatus()) ? "Розлоковано" : "Заблоковано";
	    }
	    return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		listeners.remove(l);
		
	}

	public static Map<Integer, CreditProgram> getCreditProgramsMap() {
		return creditProgramsMap;
	}

	

	
}
