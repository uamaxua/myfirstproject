﻿package tmodels;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import vo.*;

public class ModelUserAdmin implements TableModel {
	private ArrayList<User> users;
	private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
	private User user;
	
	
	public ModelUserAdmin(ArrayList<User> users) {
		this.users = users;
	}

	@Override
	public int getRowCount() {
		return users.size();
	}

	@Override
	public int getColumnCount() {
		 return 7;
	}

	@Override
	public String getColumnName(int columnIndex) {
		 switch (columnIndex) {
	        case 0:
	            return "Логін";
	        case 1:
	            return "Тип користувача";
	        case 2:
	            return "П.І.Б";
	        case 3:
	            return "Дата створення";
	        case 4:
	            return "Статус";
	        case 5:
	            return "Телефон";
	        case 6:
	            return "Еmail";
	              }
	    return "";
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		         return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		User user = users.get(rowIndex);		 
	    switch (columnIndex) {
	        case 0:
	            return user.getLogin();
	        case 1:
	            return user.getRole();
	        case 2:
	            return user.getLastName()==null ? "" : user.getLastName()+" "+user.getFirstName()+" "+user.getMiddleName();
	        case 3:
	            return user.getCreateDate();
	        case 4:
	            return "unblocked".equals(user.getStatus()) ? "розблокований" : user.getStatus() == null ?  "" : "заблокований";
	        case 5:
	            return user.getPhone();
	        case 6:
	            return user.getMail();
	        
	    }
	    return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		listeners.remove(l);
		
	}

}
