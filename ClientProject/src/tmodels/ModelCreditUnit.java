﻿package tmodels;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import vo.*;

public class ModelCreditUnit implements TableModel {
	private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
	private ArrayList<CreditUnit> creditUnits;
	
	
	
	private ModelCreditUnit() {
		}

	public ModelCreditUnit(ArrayList<CreditUnit> creditUnits) {
		this.creditUnits = creditUnits;
		
	}

	@Override
	public int getRowCount() {
		return creditUnits.size();
	}

	@Override
	public int getColumnCount() {
		 return 6;
	}

	@Override
	public String getColumnName(int columnIndex) {
		 switch (columnIndex) {
	        case 0:
	            return "№ кредиту";
	        case 1:
	            return "№ заяви";
	        case 2:
	            return "Дата початку";
	        case 3:
	            return "Дата кінця";
	        case 4:
	            return "Статус";
	        case 5:
	            return "Логін клієнта";
	        	      
	        }
	    return "";
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		 switch (columnIndex) {
	        case 0:
	            return Integer.class;
	        case 1:
	        	return Integer.class;
	        case 2:
	            return String.class;
	        case 3:
	            return String.class;
	        case 4:
	            return String.class;
	        case 5:
	            return Integer.class;
	        
	        }
	    return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		CreditUnit credit = creditUnits.get(rowIndex);
		

	    switch (columnIndex) {
	        case 0:
	            return credit.getIdCredit()==0 ? "" : credit.getIdCredit();
	        case 1:
	            return credit.getIdOrder()==0 ? "" : credit.getIdOrder();
	        case 2:
	            return credit.getDateStart().split("\\s")[0];
	        case 3:
	            return  credit.getDateEnd().split("\\s")[0];
	        case 4:
	            return credit.getStatus();
	        case 5:
	            return credit.getLogin();
	       
	    }
	    return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		listeners.remove(l);
		
	}

}
