﻿package tmodels;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import vo.*;

public class ModelUserManager implements TableModel {
	private ArrayList<User> users;
	private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
	private User user;
	
	
	public ModelUserManager(ArrayList<User> users) {
		this.users = users;
	}

	@Override
	public int getRowCount() {
		return users.size();
	}

	@Override
	public int getColumnCount() {
		 return 7;
	}

	@Override
	public String getColumnName(int columnIndex) {
		 switch (columnIndex) {
	        case 0:
	            return "Логін";
	        case 1:
	            return "П.І.Б";
	        case 2:
	            return "Телефон";
	        case 3:
	            return "Email";
	        case 4:
	            return "Дохід (грн.)";
	        case 5:
	            return "Поручитель";
	        case 6:
	            return "Статус";
	              }
	    return "";
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex) {
		        case 0:
		            return String.class;
		        case 1:
		            return String.class;
		        case 2:
		            return String.class;
		        case 3:
		            return String.class;
		        case 4:
		            return Integer.class;
		        case 5:
		            return String.class;
		        case 6:
		            return String.class;
		              }
		    return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		User user = users.get(rowIndex);		 
	    switch (columnIndex) {
	        case 0:
	            return user.getLogin();
	        case 1:
	        	return user.getLastName()==null ? "" : user.getLastName()+" "+user.getFirstName()+" "+user.getMiddleName();
	        case 2:
	        	return user.getPhone();
	        case 3:
	            return user.getMail();
	        case 4:
	        	return user.getIncome();
	        case 5:
	            return user.getGuarantee();
	        case 6:
	            return  "unblocked".equals(user.getStatus()) ? "розблокований" : user.getStatus() == null ?  "" : "заблокований";
	        
	    }
	    return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		listeners.remove(l);
		
	}

}
