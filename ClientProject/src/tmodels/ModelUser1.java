﻿package tmodels;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import vo.*;

public class ModelUser1 implements TableModel {
	private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
	private User user;
	
	
	public ModelUser1(User user) {
		this.user = user;
	}

	@Override
	public int getRowCount() {
		return 1;
	}

	@Override
	public int getColumnCount() {
		 return 5;
	}

	@Override
	public String getColumnName(int columnIndex) {
		 switch (columnIndex) {
	        case 0:
	            return "Логін";
	        case 1:
	            return "Тип користувача";
	        case 2:
	            return "Прізвище";
	        case 3:
	            return "Ім'я";
	        case 4:
	            return "По батькові";
	              }
	    return "";
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		         return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
				 
	    switch (columnIndex) {
	        case 0:
	            return user.getLogin();
	        case 1:
	            return user.getRole();
	        case 2:
	            return user.getLastName();
	        case 3:
	            return user.getFirstName();
	        case 4:
	            return user.getMiddleName();
	        
	    }
	    return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		listeners.remove(l);
		
	}

}
