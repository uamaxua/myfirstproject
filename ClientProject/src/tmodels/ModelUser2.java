﻿package tmodels;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import vo.*;

public class ModelUser2 implements TableModel {
	private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
	private User user;
	
	
	public ModelUser2(User user) {
		this.user = user;
		}

	@Override
	public int getRowCount() {
		return 1;
	}

	@Override
	public int getColumnCount() {
		 return 5;
	}

	@Override
	public String getColumnName(int columnIndex) {
		 switch (columnIndex) {
	        case 0:
	            return "Телефон";
	        case 1:
	            return "Email";
	        case 2:
	            return "Дата створення";
	        case 3:
	            return "Поручитель";
	        case 4:
	            return "Дохід(грн.)";
	        }
	    return "";
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		         
		         switch (columnIndex) {
			        case 0:
			        	return String.class;
			        case 1:
			        	return String.class;
			        case 2:
			        	return String.class;
			        case 3:
			        	return String.class;
			        case 4:
			            return user.getIncome()==0 ? String.class : Integer.class;
			    }
		         return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
				 
	    switch (columnIndex) {
	        case 0:
	            return user.getPhone() == null ? "Інформація не вказана" : user.getPhone();
	        case 1:
	            return user.getMail() == null ? "Інформація не вказана" : user.getMail();
	        case 2:
	            return user.getCreateDate();
	        case 3:
	            return user.getGuarantee() == null ? "Інформація не вказана" : user.getGuarantee();
	        case 4:
	            return user.getIncome()==0 ? "Інформація не вказана" : user.getIncome();
	    }
	    return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		listeners.remove(l);
		
	}

}
