﻿package tmodels;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import vo.*;

public class ModelOrderManager implements TableModel {
	private Set<TableModelListener> listeners = new HashSet<TableModelListener>();
	private List<Order> orders;
	private Map<Integer, CreditProgram> creditProgramsMap;
	private Map<String, User> usersNoPassword;
	
	private ModelOrderManager() {
		}

	public ModelOrderManager(List<Order> orders, Map<Integer, CreditProgram> creditProgramsMap, Map<String, User> usersNoPassword) {
		this.orders = orders;
		this.creditProgramsMap = creditProgramsMap;
		this.usersNoPassword = usersNoPassword;
	}

	@Override
	public int getRowCount() {
		return orders.size();
	}

	@Override
	public int getColumnCount() {
		 return 8;
	}

	@Override
	public String getColumnName(int columnIndex) {
		 switch (columnIndex) {
	        case 0:
	            return "№";
	        case 1:
	            return "Назва кредиту";
	        case 2:
	            return "Статус";
	        case 3:
	            return "Менеджер";
	        case 4:
	            return "Дата створення";
	        case 5:
	            return "Період";
	        case 6:
	            return "Сума";
	        case 7:
	            return "Клієнт";
	      
	        }
	    return "";
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		 switch (columnIndex) {
	        case 0:
	            return Integer.class;
	        case 1:
	            return String.class;
	        case 2:
	            return String.class;
	        case 3:
	            return String.class;
	        case 4:
	            return String.class;
	        case 5:
	            return Integer.class;
	        case 6:
	            return Integer.class;
	        case 7:
	            return Integer.class;
	        }
	    return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Order order = orders.get(rowIndex);
		CreditProgram creditProgram = creditProgramsMap.get(order.getIdCreditProgram()); 

	    switch (columnIndex) {
	        case 0:
	            return order.getIdOrder();
	        case 1:
	            return creditProgram.getName();
	        case 2:
	            return order.getStatus();
	        case 3:
	            return  order.getLoginManager();
	        case 4:
	            return order.getDateCreation().split("\\s")[0];
	        case 5:
	            return order.getDurationCredit();
	        case 6:
	            return order.getSumCredit();
	        case 7:
	            return order.getLoginClient();
	    }
	    return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
		
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		listeners.remove(l);
		
	}

}
