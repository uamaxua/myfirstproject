﻿package actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import serverconnnect.ConnectTo;
import vo.CreditProgram;
import vo.Order;
import vo.User;

public class UserChangesActionListener implements ActionListener {
	private User user;
	private JPanel panel;
	private JTextField phoneField;
	private JTextField mailField;
	private JCheckBox phoneText;
	private JCheckBox mailText;
	
	private UserChangesActionListener() {
	
	}

	public UserChangesActionListener(User user, JPanel panel, JTextField phoneField, JTextField mailField, JCheckBox phoneText, JCheckBox mailText ){ 
		this.user = user;
		this.panel = panel;
		this.phoneField = phoneField;
		this.mailField = mailField;
		this.phoneText = phoneText;
		this.mailText = mailText;
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		String password = null; 
		
		JPasswordField pf = new JPasswordField();
		
		if(phoneText.isSelected() || mailText.isSelected()){
			String phone = phoneText.isSelected() ?  phoneField.getText() : user.getPhone();
			String mail = mailText.isSelected() ? mailField.getText() : user.getMail();
			Pattern p1 = Pattern.compile("\\d+");
			Pattern p2 = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"); 
	        Matcher m1 = p1.matcher(phoneField.getText());  
	        Matcher m2 = p2.matcher(mailField.getText());
	        Boolean match1 = phoneText.isSelected() ? m1.matches() : true;
	        Boolean match2 = mailText.isSelected()?  m2.matches() : true;
	        	        
	       if ( !(match1 && match2) ){ JOptionPane.showMessageDialog(null, "Не правильно введені дані (Е-mail або телефон). Потрібно вводити тільки цифри", "Повідомлення",
					JOptionPane.WARNING_MESSAGE);  }
	       else {
		int okCxl = JOptionPane.showConfirmDialog(panel, pf, "Введіть Ваш поточний пароль", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

		if (okCxl == JOptionPane.OK_OPTION) {
		  password = new String(pf.getPassword());
		  		  
		  	ConnectTo server22 = null;
			try {
				server22 = new ConnectTo();
			
			ObjectOutputStream outStream = new ObjectOutputStream(server22.clSocket.getOutputStream());
			ObjectInputStream inputStream = new ObjectInputStream(server22.clSocket.getInputStream());
			outStream.writeObject("update_user");
			ObjectOutputStream outStreamObj = new ObjectOutputStream(server22.clSocket.getOutputStream());
			
			User updatedUser = new User(user.getLogin(), password, user.getRole(), user.getFirstName(),
						user.getLastName(), user.getMiddleName(), phone, mail,
						user.getCreateDate(), user.getIncome(), user.getGuarantee(), user.getStatus());
			 
			outStreamObj.writeObject(updatedUser); 
			String input = (String) inputStream.readObject();
			
			if("yes".equals(input)) 
				JOptionPane.showMessageDialog(null, "Дані було успішно оновлені", "Повідомлення",
					JOptionPane.WARNING_MESSAGE); 
			else
			
				JOptionPane.showMessageDialog(null, "Неправильний пароль. Повторіть, будь ласка, спробу.", "Повідомлення",
						JOptionPane.WARNING_MESSAGE); 
			
			server22.clSocket.close();
				
			} catch (UnknownHostException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (ClassNotFoundException e1) {
				
				e1.printStackTrace();
			}
		  
		 
		}
		
		}
		
	}
	}
}
