﻿package actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import gui.AboutProgrammDialog;

public class AboutActionListener implements ActionListener {
	
	@Override
	public void actionPerformed(ActionEvent e) {
		AboutProgrammDialog.getInstance().setVisible(true);
		}

}

