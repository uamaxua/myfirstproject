﻿package actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import serverconnnect.ConnectTo;
import vo.CreditProgram;
import vo.Order;

public class OrderActionListener implements ActionListener {
	private CreditProgram selectedCreditProgram;
	private String login;
	private JTextField sumField;
	private JTextField periodField;
	private int period;
	private int sum;
	private int maxSum;
	private int minSum;
	private int maxPeriod;
	
	private OrderActionListener() {
	
	}

	public OrderActionListener(CreditProgram selectedCreditProgram, String login, JTextField sumField, JTextField periodField, 
			int maxSum, int minSum, int maxPeriod) {
		this.selectedCreditProgram = selectedCreditProgram;
		this.login = login;
		this.sumField = sumField;
		this.periodField = periodField;
		this.maxSum = maxSum;
		this.minSum = minSum;
		this.maxPeriod = maxPeriod;
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		 Pattern p = Pattern.compile("\\d+");  
	        Matcher m1 = p.matcher(periodField.getText());  
	        Matcher m2 = p.matcher(sumField.getText());
	        if ( !(m1.matches() && m1.matches()) ) JOptionPane.showMessageDialog(null, "Не правильно введені дані (сума або період кредиту). Потрібно вводити тільки цифри", "Повідомлення",
					JOptionPane.WARNING_MESSAGE); 
	        else {
	        period = Integer.valueOf(periodField.getText());
	    	sum = Integer.valueOf(sumField.getText());
	        	if(!(period >= 1 && period <= maxPeriod && sum >= minSum && sum <= maxSum))
	        		JOptionPane.showMessageDialog(null, "Не правильно введені дані для контретної кредитної програми. Період або сума кредиту перевищують дозволені ліміти", "Повідомлення",
	    					JOptionPane.WARNING_MESSAGE); 
	        	else {
	        
		String input = null;
		ConnectTo server1 = null;
		
		try {
			server1 = new ConnectTo();
			ObjectOutputStream outStream = new ObjectOutputStream(server1.clSocket.getOutputStream());
			ObjectInputStream inputStream = new ObjectInputStream(server1.clSocket.getInputStream());
			outStream.writeObject("user_order_write");
					
			ObjectOutputStream outStreamObj = new ObjectOutputStream(server1.clSocket.getOutputStream());
			Order order = new Order(999, selectedCreditProgram.getIdCreditProgram(), "В черзі", " ", "date", period, sum, login);
			outStreamObj.writeObject(order);
			input = (String) inputStream.readObject();
			if("yes".equals(input)) 
				JOptionPane.showMessageDialog(null, "Заява на оформлення кредиту була успішно сформована", "Повідомлення" ,
						JOptionPane.INFORMATION_MESSAGE);
			else
				JOptionPane.showMessageDialog(null, "Не можливо оформити заяву. Звертайтесь за інформацією до менеджера", "Повідомлення",
						JOptionPane.WARNING_MESSAGE);
			
			
			server1.clSocket.close();
				
			
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		
		
		
	        	}
	        }	
	}

}
