﻿package actions;

import gui.MainGuiAdmin;
import gui.MainGuiManager;
import gui.MainGuiUser;
import gui.PasswordDialog;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.UnknownHostException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import serverconnnect.ConnectTo;

public class PasswordActionListener implements ActionListener {
	private String login;
	private String password;
	private PasswordDialog pswDialog;
	private JTextField loginField;
	private JPasswordField pswField;
	
		private PasswordActionListener() {
		
	}

		public PasswordActionListener(PasswordDialog pswDialog,
				JTextField loginField, JPasswordField pswField) {
			this.pswDialog = pswDialog;
			this.loginField = loginField;
			this.pswField = pswField;
		}


		@Override
		public void actionPerformed(ActionEvent e) {
			ConnectTo server = null;
			try {
				server = new ConnectTo();
			} catch (UnknownHostException e2) {
				e2.printStackTrace();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			ObjectOutputStream outStream = null;
			try {
				outStream = new ObjectOutputStream(server.clSocket.getOutputStream());
			} catch (IOException e3) {
				
			}
			catch (NullPointerException e3) {
				System.exit(0);	
			}
			
			String login = loginField.getText().trim();
			String password = String.valueOf(pswField.getPassword()).trim();
			
			try {
				outStream.writeObject("password_identity");
			
			outStream.writeObject(login);
			outStream.writeObject(password);
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			System.out.println("Login and password were sent");
			
			
			try {
				ObjectInputStream  inStream = new ObjectInputStream(server.clSocket.getInputStream());
				String answer = (String) inStream.readObject();
				
						System.out.println("answer="+answer);
						
				if("allow".equals(answer)){
					String role = (String) inStream.readObject();
					String fio = (String) inStream.readObject();
					System.out.println(fio);
					if("адміністратор".equals(role)){
						new MainGuiAdmin(login, role, fio);
						pswDialog.dispose();
					}
					if("клієнт".equals(role)){
						new MainGuiUser(login, role, fio);
						pswDialog.dispose();
					}
					if("менеджер".equals(role)){
						new MainGuiManager(login, role, fio);
						pswDialog.dispose();
					}
					server.clSocket.close();
				} else {
					
					
					JOptionPane.showMessageDialog(null, "Не правильний дані аутентифікації або ваш обліковий запис заблоковано."
							+ " За інформацією звертайтесь до менеджера банку", "Повідомлення" , JOptionPane.WARNING_MESSAGE);
					server.clSocket.close();
				}
				
			} catch (HeadlessException | IOException e1) {
				
				e1.printStackTrace();
				
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			}
						
		}
		
	

}
