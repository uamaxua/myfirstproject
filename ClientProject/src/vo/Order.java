﻿package vo;

import java.io.Serializable;

public class Order implements Serializable {
	
	private int idOrder;
	private int idCreditProgram;
	private String status;
	private String LoginManager;
	private String DateCreation;
	private int DurationCredit;
	private int SumCredit;
	private String LoginClient;
	
	private Order() {
		
	}

	public Order(int idOrder, int idCreditProgram, String status,
			String loginManager, String dateCreation, int durationCredit,
			int sumCredit, String loginClient) {
		this.idOrder = idOrder;
		this.idCreditProgram = idCreditProgram;
		this.status = status;
		LoginManager = loginManager;
		DateCreation = dateCreation;
		DurationCredit = durationCredit;
		SumCredit = sumCredit;
		LoginClient = loginClient;
	}

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	public int getIdCreditProgram() {
		return idCreditProgram;
	}

	public void setIdCreditProgram(int idCreditProgram) {
		this.idCreditProgram = idCreditProgram;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLoginManager() {
		return LoginManager;
	}

	public void setLoginManager(String loginManager) {
		LoginManager = loginManager;
	}

	public String getDateCreation() {
		return DateCreation;
	}

	public void setDateCreation(String dateCreation) {
		DateCreation = dateCreation;
	}

	public int getDurationCredit() {
		return DurationCredit;
	}

	public void setDurationCredit(int durationCredit) {
		DurationCredit = durationCredit;
	}

	public int getSumCredit() {
		return SumCredit;
	}

	public void setSumCredit(int sumCredit) {
		SumCredit = sumCredit;
	}

	public String getLoginClient() {
		return LoginClient;
	}

	public void setLoginClient(String loginClient) {
		LoginClient = loginClient;
	}


}
