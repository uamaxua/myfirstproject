﻿package vo;

import java.io.Serializable;

public class CreditProgram implements Serializable {
	
	private int idCreditProgram;
	private String name;
	private String shortDescription;
	private String fullDescription;
	private int maxPeriod;
	private int maxMoneyAmount;
	private int minMoneyAmoun;
	private float percent;
	private String status;

	private CreditProgram(){
		
	}

	public CreditProgram(int idCreditProgram, String name,
			String shortDescription, String fullDescription, int maxPeriod,
			int maxMoneyAmount,  float percent, int minMoneyAmoun, String status) {
		this.idCreditProgram = idCreditProgram;
		this.name = name;
		this.shortDescription = shortDescription;
		this.fullDescription = fullDescription;
		this.maxPeriod = maxPeriod;
		this.maxMoneyAmount = maxMoneyAmount;
		this.percent = percent;
		this.minMoneyAmoun = minMoneyAmoun;
		this.status = status;
	}

	public int getIdCreditProgram() {
		return idCreditProgram;
	}

	public void setIdCreditProgram(int idCreditProgram) {
		this.idCreditProgram = idCreditProgram;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getFullDescription() {
		return fullDescription;
	}

	public void setFullDescription(String fullDescription) {
		this.fullDescription = fullDescription;
	}

	public int getMaxPeriod() {
		return maxPeriod;
	}

	public void setMaxPeriod(int maxPeriod) {
		this.maxPeriod = maxPeriod;
	}

	public int getMaxMoneyAmount() {
		return maxMoneyAmount;
	}

	public void setMaxMoneyAmount(int maxMoneyAmount) {
		this.maxMoneyAmount = maxMoneyAmount;
	}

	
	public int getMinMoneyAmoun() {
		return minMoneyAmoun;
	}

	public void setMinMoneyAmoun(int minMoneyAmoun) {
		this.minMoneyAmoun = minMoneyAmoun;
	}

	public float getPercent() {
		return percent;
	}

	public void setPercent(float percent) {
		this.percent = percent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	
}
