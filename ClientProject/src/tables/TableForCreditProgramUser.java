﻿package tables;

import java.awt.Dimension;

import javax.swing.JTable;
import javax.swing.table.TableModel;

public class TableForCreditProgramUser extends JTable {
	private TableModel model;

	private TableForCreditProgramUser() {
		
	}

	public TableForCreditProgramUser(TableModel model) {
		super(model);
		this.model = model;
		getTableHeader().setReorderingAllowed(false);
		setPreferredScrollableViewportSize(new Dimension(990, 150));
		getColumnModel().getColumn(0).setMinWidth(30);
		getColumnModel().getColumn(0).setMaxWidth(30);
		getColumnModel().getColumn(1).setMinWidth(190);
		getColumnModel().getColumn(1).setMaxWidth(190);
		getColumnModel().getColumn(3).setMinWidth(30);
		getColumnModel().getColumn(3).setMaxWidth(80);
		getColumnModel().getColumn(4).setMinWidth(100);
		getColumnModel().getColumn(4).setMaxWidth(100);
		getColumnModel().getColumn(5).setMinWidth(100);
		getColumnModel().getColumn(5).setMaxWidth(100);
		getColumnModel().getColumn(6).setMinWidth(80);
		getColumnModel().getColumn(6).setMaxWidth(80);
		
		
	}
	
	
	

}
