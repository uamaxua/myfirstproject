﻿package tables;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTable;
import javax.swing.table.TableModel;

public class TableForUser extends JTable {
	private TableModel model;

	private TableForUser() {
		
	}

	public TableForUser(TableModel model) {
		super(model);
		this.model = model;
		getTableHeader().setReorderingAllowed(false);
		getTableHeader().setBackground(Color.LIGHT_GRAY);
	      
		setPreferredScrollableViewportSize(new Dimension(990, 20));
		
	}
	
	
	

}
