﻿package tables;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTable;
import javax.swing.table.TableModel;

public class TableForOrders extends JTable {
	private TableModel model;

	private TableForOrders() {
		
	}

	public TableForOrders(TableModel model) {
		super(model);
		this.model = model;
		getTableHeader().setReorderingAllowed(false);
		getTableHeader().setBackground(Color.LIGHT_GRAY);
		
		
		getColumnModel().getColumn(4).setMinWidth(110);
		getColumnModel().getColumn(4).setMaxWidth(110);
		getColumnModel().getColumn(5).setMinWidth(80);
		getColumnModel().getColumn(5).setMaxWidth(80);  
		getColumnModel().getColumn(6).setMinWidth(80);
		getColumnModel().getColumn(6).setMaxWidth(80);  
		setPreferredScrollableViewportSize(new Dimension(990, 20));
		
	}
	
	
	

}
