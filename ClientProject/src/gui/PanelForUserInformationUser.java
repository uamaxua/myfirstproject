﻿package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableModel;

import actions.UserChangesActionListener;
import serverconnnect.ConnectTo;
import tables.TableForCreditProgramAdmin;
import tables.TableForOrders;
import tables.TableForUser;
import tmodels.ModelCreditProgramAdmin;
import tmodels.ModelCreditUnit;
import tmodels.ModelOrder;
import tmodels.ModelUser1;
import tmodels.ModelUser2;
import vo.CreditProgram;
import vo.CreditUnit;
import vo.Order;
import vo.User;

public class PanelForUserInformationUser extends JPanel {
	private User user;
	private List<Order> orders;
	private Map<Integer, CreditProgram> creditProgramsMap;
	private Map<String, User> usersNoPassword;
	private ArrayList<CreditUnit> creditUnits;
	private PanelForUserInformationUser(){
			}
	
	public PanelForUserInformationUser(String login) throws IOException{
	//*****************************************************************************
	ConnectTo server = null;
			try {
				server = new ConnectTo();
				ObjectOutputStream outStream = new ObjectOutputStream(server.clSocket.getOutputStream());
				outStream.writeObject("user_client_get");
				outStream.writeObject(login);
				ObjectInputStream inStreamObject = new ObjectInputStream(server.clSocket.getInputStream());
				user = (User) (inStreamObject.readObject());
				ObjectInputStream inStreamObject1 = new ObjectInputStream(server.clSocket.getInputStream());
				orders = (ArrayList<Order>) (inStreamObject1.readObject());
				ObjectInputStream inStreamObject2 = new ObjectInputStream(server.clSocket.getInputStream());
				usersNoPassword = (Map<String, User>) (inStreamObject2.readObject());
				inStreamObject2 = new ObjectInputStream(server.clSocket.getInputStream());
				creditUnits = (ArrayList<CreditUnit>) (inStreamObject2.readObject());
		
		} catch (UnknownHostException e) {
				e.printStackTrace();
		} catch (IOException e) {
				e.printStackTrace();
		} catch (ClassNotFoundException e) {
				e.printStackTrace();
		}
			finally{
				server.clSocket.close();
			}
			
	//*****************************************************************************
			System.out.println("orders="+orders);
			TableModel model1 = new ModelUser1(user);
			TableModel model2 = new ModelUser2(user);
			JPanel panel = new JPanel();
			panel.setPreferredSize(new Dimension(990, 120));
			panel.setLayout(new BorderLayout());
			panel.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Особиста інформація користувача")));
			add(panel);
			
			
			JTable tableForUser1 = new TableForUser(model1);
			JTable tableForUser2 = new TableForUser(model2);
			
			JScrollPane scrollPane1 = new JScrollPane(tableForUser1);
			JScrollPane scrollPane2 = new JScrollPane(tableForUser2);
			
			panel.add(scrollPane1, BorderLayout.NORTH );
			panel.add(scrollPane2, BorderLayout.SOUTH );
			
	//********************************************************************************	
			creditProgramsMap = ModelCreditProgramAdmin.getCreditProgramsMap();
			TableModel modelOrders = new ModelOrder(orders, creditProgramsMap, usersNoPassword);
			
			JPanel panel1 = new JPanel();
			panel1.setPreferredSize(new Dimension(990, 145));
			panel1.setLayout(new BorderLayout());
			panel1.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Ваші подані заявки на кредит")));
			add(panel1);
			JTable tableForOrder = new TableForOrders(modelOrders);
			
			JScrollPane scrollPaneForOrder = new JScrollPane(tableForOrder);
			scrollPaneForOrder.setPreferredSize(new Dimension(990, 120));
			scrollPaneForOrder.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
						
			panel1.add(scrollPaneForOrder, BorderLayout.NORTH );
			
			
	//********************************************************************************	
			

			creditProgramsMap = ModelCreditProgramAdmin.getCreditProgramsMap();
			TableModel modelOrders1 = new ModelCreditUnit(creditUnits);
			
			JPanel panel2 = new JPanel();
			panel2.setPreferredSize(new Dimension(990, 110));
			panel2.setLayout(new BorderLayout());
			panel2.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Ваші оформлені кредити")));
			add(panel2);
			JTable tableForOrder1 = new JTable(modelOrders1);
			
			JScrollPane scrollPaneForOrder1 = new JScrollPane(tableForOrder1);
			scrollPaneForOrder1.setPreferredSize(new Dimension(990, 85));
			scrollPaneForOrder1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
						
			panel2.add(scrollPaneForOrder1, BorderLayout.NORTH );
			
			
		
			
//********************************************************************************	
			
			JPanel panelForInfoChange = new JPanel();
			panelForInfoChange.setPreferredSize(new Dimension(990, 110));
			panelForInfoChange.setLayout(null);
			panelForInfoChange.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Змінити особисту інформацію")));
			add(panelForInfoChange);
			
			JCheckBox phoneText = new JCheckBox("Введіть ваш телефон:");
			JTextField phoneField = new JTextField();
			
			phoneText.setLocation(15,15);
			phoneText.setSize(180, 25);
			panelForInfoChange.add(phoneText);
			
			phoneField.setLocation(15, 40);
			phoneField.setSize(200, 20);
			phoneField.setVisible(false);
			panelForInfoChange.add(phoneField);
			
			
			JCheckBox mailText = new JCheckBox("Введіть ваш E-mail:");
			JTextField mailField = new JTextField();
			
			mailText.setLocation(225,15);
			mailText.setSize(180, 25);
			panelForInfoChange.add(mailText);
			
			mailField.setLocation(225, 40);
			mailField.setSize(200, 20);
			mailField.setVisible(false);
			panelForInfoChange.add(mailField);
								
			JLabel WarningText = new JLabel("Для того щоб змінити іншу інформацію, звертайтесь до менеджера банку.");
			WarningText.setLocation(480, 35);
			WarningText.setSize(500, 25);
			panelForInfoChange.add(WarningText);
			
			JButton buttonChange = new JButton("Змінити дані");
			buttonChange.setLocation(160, 65);
			buttonChange.setSize(110, 25);
			panelForInfoChange.add(buttonChange);
			buttonChange.addActionListener(new UserChangesActionListener(user, this, phoneField, mailField, phoneText, mailText));
			
			
			
			mailText.addItemListener(new ItemListener() {
			    public void itemStateChanged(ItemEvent e) {
			      if  (mailText.isSelected()) mailField.setVisible(true);
			      else
			    	  mailField.setVisible(false);
			    }
			});
			
			phoneText.addItemListener(new ItemListener() {
			    public void itemStateChanged(ItemEvent e) {
			      if  (phoneText.isSelected()) phoneField.setVisible(true);
			      else
			    	  phoneField.setVisible(false);
			    }
			});
		
				
	//********************************************************************************				
			
			
	}
}
