﻿package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import actions.OrderActionListener;
import serverconnnect.ConnectTo;
import vo.CreditProgram;
import vo.Order;
import tables.TableForCreditProgramAdmin;
import tmodels.*;

public class PanelForCreditProgramManager extends JPanel {
	private ArrayList<CreditProgram> creditPrograms;
	private ConnectTo server;
	private int maxSum;
	private int minSum;
	private int maxPeriod;
	private CreditProgram selectedCreditProgram;
	private String login;
	private OrderActionListener orderActionListener;
	
	
	private PanelForCreditProgramManager() {
		
	}


	public PanelForCreditProgramManager(String login) throws IOException {
				
//*****************************************************************************
		
		try {
			server = new ConnectTo();
			ObjectOutputStream outStream = new ObjectOutputStream(server.clSocket.getOutputStream());
			outStream.writeObject("credit_program_get");
			ObjectInputStream inStreamObject = new ObjectInputStream(server.clSocket.getInputStream());
			creditPrograms = (ArrayList<CreditProgram>) (inStreamObject.readObject());
	
	
//*****************************************************************************
		TableModel model = new ModelCreditProgramAdmin(creditPrograms);
		JTable tableForCreditProgram = new TableForCreditProgramAdmin(model);
		
		JScrollPane scrollPaneForCredit = new JScrollPane(tableForCreditProgram);
		scrollPaneForCredit.setPreferredSize(new Dimension(990, 180));
		scrollPaneForCredit.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPaneForCredit.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Кредитні програми")));
		add(scrollPaneForCredit);
		
//********************************************************************************	

				JPanel panelForFullDesc = new JPanel();
				panelForFullDesc.setLayout(new FlowLayout(FlowLayout.LEFT)); 
				panelForFullDesc.setPreferredSize(new Dimension(990, 110));
				panelForFullDesc.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Повний опис кредитної програми")));
				JTextArea TAreaForFullDesc = new JTextArea("Оберіть програму кредитування з таблиці.", 2, 86);
				TAreaForFullDesc.setLineWrap(isEnabled());
				TAreaForFullDesc.setBackground(panelForFullDesc.getBackground());
				TAreaForFullDesc.setWrapStyleWord(true);
				TAreaForFullDesc.setEditable(false);
				panelForFullDesc.add(TAreaForFullDesc);
				add(panelForFullDesc);

							
//**********************************************************************************	
			
		ListSelectionModel selModel = tableForCreditProgram.getSelectionModel();
		selModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);  
		selModel.addListSelectionListener(new ListSelectionListener() { 
				     
               public void valueChanged(ListSelectionEvent e) {
                   int selectRow = tableForCreditProgram.getSelectedRow();
                   TAreaForFullDesc.setText(creditPrograms.get(selectRow).getFullDescription());
                   selectedCreditProgram = creditPrograms.get(selectRow);
                   maxSum = creditPrograms.get(selectRow).getMaxMoneyAmount();
                   minSum = creditPrograms.get(selectRow).getMinMoneyAmoun();
                   maxPeriod = creditPrograms.get(selectRow).getMaxPeriod();
                  
                  
                   
           		
               }               
          });
          
//**********************************************************************************
		
		
		} catch (UnknownHostException e) {
			e.printStackTrace();
	} catch (IOException e) {
			e.printStackTrace();
	} catch (ClassNotFoundException e) {
			e.printStackTrace();
	}
		finally{
			server.clSocket.close();
		}	
		
	}
 
}
