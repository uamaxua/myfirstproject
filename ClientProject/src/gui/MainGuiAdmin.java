﻿package gui;

import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class MainGuiAdmin extends JFrame {
	private JTabbedPane tabPane;
	private String role = "";
	private String fio = "";
	private String login = "";
	
	private MainGuiAdmin(){
		
	}

	public MainGuiAdmin(String login, String role, String fio) throws IOException{
		this.role = role;
		this.fio = fio;
		this.login = login;
		setTitle("ПАТ КБ \"ДемоБанк\" Кредитування Фізичних Осіб "+AboutProgrammDialog.PROG_VERSION+"  "+role+": "+fio);
		setResizable(false);
		tabPane = new JTabbedPane();
		//*********************************
		tabPane.addTab("Редагування кредитних програм", new PanelForCreditProgramAdmin(login));
		tabPane.addTab("Управління користувачами", new PanelForUserInformationAdmin(login));
		
		//*********************************
		add(tabPane);
		setSize(1024, 600);
		setLocationRelativeTo(null);
		setJMenuBar(new TopMenuBar(tabPane, login, role));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	
	}
}
