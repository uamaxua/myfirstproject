﻿package gui;

import java.awt.Color;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

public class AboutProgrammDialog extends JDialog  {
	public static final String PROG_VERSION = "(Version: 0.1 alpha)"; 	
	private JTextPane pane = new JTextPane();
	private JTextArea text = new JTextArea(" Програма для кредитування: \n "+PROG_VERSION
				+"\n 01.08.2014 \n Виконується Java машина версії 1.8.0"
				+ "\n Розробник: Печенюк Максим \n Контакти: uamaxua@gmail.com");
	private static AboutProgrammDialog instance;
	
	private AboutProgrammDialog(){
		setTitle("Про программу");
		setSize(250, 130);
		setLocationRelativeTo(null);
		pane.add(text);
		add(pane);
		setBackground(Color.WHITE);
		add(text);
		setResizable(false);
		text.setEditable(false);
			
	}
	
	public static AboutProgrammDialog getInstance()
    {
        if (instance == null)
        {
            instance = new AboutProgrammDialog();
        }
        return instance;

}
	
}