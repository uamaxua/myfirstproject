﻿package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;

import actions.*;

public class TopMenuBar extends JMenuBar {
	private JTabbedPane tabPane;
	private String login = "";
	private String role;
	
	private JMenu userMenu = new JMenu("Користувач");
		private JMenuItem subMenu11 = new JMenuItem("Змінити пароль");
		
		private JMenuItem subMenu13 = new JMenuItem("Вихід");
	private JMenu updateMenu = new JMenu("Оновоти дані");
		private JMenuItem subMenu21 = new JMenuItem("Оновити");
	private	JMenu helpMenu = new JMenu("Допомога");  
		private JMenuItem subMenu31 = new JMenuItem("Інструкція користувача");
		private JMenuItem subMenu32 = new JMenuItem("Про програму");
	
	
		
	private TopMenuBar() {
			
		}



	public TopMenuBar(JTabbedPane tabPane, String login, String role)  {
		this.tabPane = tabPane;
		this.login = login;
		this.role = role;
		subMenu11.setEnabled(false);
				
	       	userMenu.add(subMenu11);
	       	userMenu.addSeparator();
	       
	       	subMenu13.addActionListener(new ExitActionListener());
	       	userMenu.add(subMenu13);
		 	
	       	updateMenu.add(subMenu21);
	       	
        	helpMenu.add(subMenu31);
        	subMenu31.setEnabled(false);
        	helpMenu.addSeparator();
        	helpMenu.add(subMenu32);
        	subMenu32.addActionListener(new AboutActionListener());
        	        	
        	add(userMenu);
        	add(helpMenu);
        	add(updateMenu);
       
        	subMenu21.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
				int index =	tabPane.getSelectedIndex();
										
					tabPane.removeAll();
					try {
						if("клієнт".equals(role)){
						tabPane.addTab("Вибір кредиту", new PanelForCreditProgramUser(login));
						tabPane.addTab("Інформація користувача", new PanelForUserInformationUser(login));
						tabPane.setSelectedIndex(index);
						}
						
						if("адміністратор".equals(role)){
							tabPane.addTab("Редагування кредитних програм", new PanelForCreditProgramAdmin(login));
							tabPane.addTab("Управління користувачами", new PanelForUserInformationAdmin(login));
							tabPane.setSelectedIndex(index);
						}
						
						if("менеджер".equals(role)){
							tabPane.addTab("Опис кредитних програм", new PanelForCreditProgramManager(login));
							tabPane.addTab("Управління клієнтами", new PanelForUserInformationManager(login));
							tabPane.addTab("Управління заявками на кредити", new PanelForManager(login, tabPane));
							tabPane.setSelectedIndex(index);
						}
							
						
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					
				}
			}); 	
        	
        	
	}
	
}
