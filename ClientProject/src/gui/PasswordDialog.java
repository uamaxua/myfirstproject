﻿package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import serverconnnect.*;
import actions.*;

public class PasswordDialog extends JDialog {
		
	private JLabel logText = new JLabel("Введіть логін:");
	private JTextField loginField = new JTextField(20);
	private JLabel pswText = new JLabel("Введіть пароль:");
	private JPasswordField pswField = new JPasswordField(20);
	private JButton button = new JButton("Вхід");
	private JPanel panel = new JPanel();
	String login = loginField.getText().trim();
	String password = String.valueOf(pswField.getPassword()).trim();
	
	
	
	public PasswordDialog() throws IOException {
		setTitle("Введення логіну та паролю");
		
		setSize(300,200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		panel.setLayout(new GridLayout(4,1));
		panel.setBorder(new EmptyBorder(20, 20, 20, 20));
		panel.add(logText );
		panel.add(loginField);
		panel.add(pswText);
		panel.add(pswField);
		add(panel);
		add(button);
		
				
		button.addActionListener(new PasswordActionListener(this, loginField, pswField) );
		pswField.addActionListener(new PasswordActionListener(this, loginField, pswField));
		
	}

	
}

