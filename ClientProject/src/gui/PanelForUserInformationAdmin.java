﻿package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import limits.JTextFieldLimit;
import actions.UserChangesActionListener;
import serverconnnect.ConnectTo;
import tables.TableForCreditProgramAdmin;
import tables.TableForOrders;
import tables.TableForUser;
import tmodels.ModelCreditProgramAdmin;
import tmodels.ModelOrder;
import tmodels.ModelUser1;
import tmodels.ModelUser2;
import tmodels.ModelUserAdmin;
import vo.CreditProgram;
import vo.Order;
import vo.User;

public class PanelForUserInformationAdmin extends JPanel {
	private User user;
	private ArrayList<User> users;
	private Map<Integer, CreditProgram> creditProgramsMap;
	private Map<String, User> usersNoPassword;
	private JTable tableForUser1;
	private JRadioButton updateUser;
	private String block = "blocked";
	private String role = "менеджер";
	private User selectedUser;
	private String password;
	
	private PanelForUserInformationAdmin(){
			}
	
	public PanelForUserInformationAdmin(String login) throws IOException{
		
			User user = new User();
			ArrayList<User> temp = new ArrayList<>();
			temp.add(user);
			TableModel model1 = new ModelUserAdmin(temp);
			
			JPanel panel = new JPanel();
			panel.setPreferredSize(new Dimension(990, 120));
			panel.setLayout(new BorderLayout());
			panel.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Особиста інформація користувача")));
			add(panel);
			
			
			tableForUser1 = new TableForUser(model1);
			JScrollPane scrollPane1 = new JScrollPane(tableForUser1);
			scrollPane1.setPreferredSize(new Dimension(960, 95));
			
			panel.add(scrollPane1, BorderLayout.NORTH );
			
			
	//********************************************************************************	
		
			
			
			JPanel panel1 = new JPanel();
			panel1.setPreferredSize(new Dimension(990, 80));
			panel1.setLayout(null);
			panel1.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Пошук користувача системи")));
			add(panel1);
			
			JLabel loginText = new JLabel("Введіть логін:");
			JTextField loginField = new JTextField();
			
			loginText.setLocation(15,15);
			loginText.setSize(250, 25);
			panel1.add(loginText);
			
			loginField.setLocation(15, 40);
			loginField.setSize(200, 20);
			loginField.setDocument(new JTextFieldLimit(30));
			panel1.add(loginField);
			
			
			JLabel lastNameText = new JLabel("Введіть прізвище:");
			JTextField lastNameField = new JTextField();
			
			lastNameText.setLocation(235,15);
			lastNameText.setSize(250, 25);
			panel1.add(lastNameText);
			
			lastNameField.setLocation(235, 40);
			lastNameField.setSize(200, 20);
			lastNameField.setDocument(new JTextFieldLimit(30));
			panel1.add(lastNameField);
			
			
			JLabel firstNameText = new JLabel("Введіть ім'я:");
			JTextField firstNameField = new JTextField();
			
			firstNameText.setLocation(455,15);
			firstNameText.setSize(250, 25);
			panel1.add(firstNameText);
			
			firstNameField.setLocation(455, 40);
			firstNameField.setSize(200, 20);
			firstNameField.setDocument(new JTextFieldLimit(30));
			panel1.add(firstNameField);
			
			
			JButton findButton = new JButton("Пошук");
			
			findButton.setLocation(675,40);
			findButton.setSize(100, 20);
			panel1.add(findButton);
		
			findButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					tableForUser1.getSelectionModel().clearSelection();
					String login = loginField.getText().trim();
					String firstName = firstNameField.getText().trim();
					String lastName = lastNameField.getText().trim();
				ConnectTo server = null;
							try {
								server = new ConnectTo();
								ObjectOutputStream outStream = new ObjectOutputStream(server.clSocket.getOutputStream());
								outStream.writeObject("find_user");
								outStream.writeObject(login);
								outStream.writeObject(firstName);
								outStream.writeObject(lastName);
								ObjectInputStream inStream = new ObjectInputStream(server.clSocket.getInputStream());
								
								ObjectInputStream inStreamObj = new ObjectInputStream(server.clSocket.getInputStream());
								users = (ArrayList<User>) (inStreamObj.readObject());
															
								String s = (String) inStream.readObject();
								
								server.clSocket.close();
						
						} catch (UnknownHostException e1) {
								e1.printStackTrace();
						} catch (IOException e1) {
								e1.printStackTrace();
						} catch (ClassNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
							tableForUser1.setModel(new ModelUserAdmin(users));			
				}
			});
	
//*********************************** RADIO *********************************************				
			JPanel panelForAddProgram = new JPanel();
			panelForAddProgram.setLayout(null); 
			panelForAddProgram.setPreferredSize(new Dimension(990, 240));
			panelForAddProgram.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Додати/оновити "
					+ "користувача системи")));
			
			add(panelForAddProgram);
			
			JRadioButton createUser   = new JRadioButton("Створити нового користувача");
			createUser.setSelected(true);
			updateUser  = new JRadioButton("Оновити інформацію поточного користувача");
			ButtonGroup bgroup = new ButtonGroup();
			bgroup.add(createUser);
			bgroup.add(updateUser);
			createUser.setLocation(250,10);
			createUser.setSize(240, 25);
			panelForAddProgram.add(createUser);
			updateUser.setLocation(550,10);
			updateUser.setSize(290, 20);
			
			panelForAddProgram.add(updateUser);
//******************************END RADIO**************************************************************			

//******************************ELEMENTS**************************************************************				
			JLabel loginText1 = new JLabel("Логін:");
			JTextField loginField1 = new JTextField();
			
			loginText1.setLocation(15,35);
			loginText1.setSize(250, 25);
			panelForAddProgram.add(loginText1);
			
			loginField1.setLocation(15, 60);
			loginField1.setSize(200, 20);
			loginField1.setDocument(new JTextFieldLimit(30));
			panelForAddProgram.add(loginField1);
			
			
			JCheckBox passwordText = new JCheckBox("Пароль:");
			JTextField passwordField = new JTextField();
			passwordField.setVisible(true);
			passwordText.setSelected(true);
			passwordText.setEnabled(false);
			
			passwordText.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(passwordText.isSelected()) passwordField.setVisible(true);
					else
						passwordField.setVisible(false);
					
				}
			});
			
			
			passwordText.setLocation(235,35);
			passwordText.setSize(150, 25);
			panelForAddProgram.add(passwordText);
			
			passwordField.setLocation(235, 60);
			passwordField.setSize(200, 20);
			passwordField.setDocument(new JTextFieldLimit(30));
			panelForAddProgram.add(passwordField);
			
			
			JLabel roleText = new JLabel("Тип користувача:");
			JComboBox roleField = new JComboBox();
			roleField.addItem("адміністратор");
			roleField.addItem("менеджер");
			roleText.setLocation(455,35);
			roleText.setSize(250, 25);
			panelForAddProgram.add(roleText);
			
	
			roleField.setLocation(455, 60);
			roleField.setSize(200, 20);
			panelForAddProgram.add(roleField);
			
			
			JLabel firstText1 = new JLabel("Ім'я:");
			JTextField firstField = new JTextField();
			
			firstText1.setLocation(15,90);
			firstText1.setSize(250, 25);
			panelForAddProgram.add(firstText1);
			
			firstField.setLocation(15, 115);
			firstField.setSize(200, 20);
			firstField.setDocument(new JTextFieldLimit(30));
			panelForAddProgram.add(firstField);
			
			JLabel lastText = new JLabel("Прізвище:");
			JTextField lastField = new JTextField();
			
			lastText.setLocation(235,95);
			lastText.setSize(250, 25);
			panelForAddProgram.add(lastText);
			
			lastField.setLocation(235, 115);
			lastField.setSize(200, 20);
			lastField.setDocument(new JTextFieldLimit(30));
			panelForAddProgram.add(lastField);
			
			
			JLabel middleText = new JLabel("По батькові:");
			JTextField middleField = new JTextField();
			
			middleText.setLocation(455, 90);
			middleText.setSize(250, 25);
			panelForAddProgram.add(middleText);
			
			middleField.setLocation(455, 115);
			middleField.setSize(200, 20);
			middleField.setDocument(new JTextFieldLimit(30));
			panelForAddProgram.add(middleField);
			
			
			JLabel phoneText = new JLabel("Телефон:");
			JTextField phoneField = new JTextField();
			
			phoneText.setLocation(15,155);
			phoneText.setSize(250, 25);
			panelForAddProgram.add(phoneText);
			
			phoneField.setLocation(15, 180);
			phoneField.setSize(200, 20);
			phoneField.setDocument(new JTextFieldLimit(30));
			panelForAddProgram.add(phoneField);
			
			JLabel mailText = new JLabel("Email:");
			JTextField mailField = new JTextField();
			
			mailText.setLocation(235,155);
			mailText.setSize(250, 25);
			panelForAddProgram.add(mailText);
			
			mailField.setLocation(235, 180);
			mailField.setSize(200, 20);
			mailField.setDocument(new JTextFieldLimit(30));
			panelForAddProgram.add(mailField);
			
			
			JLabel statusText = new JLabel("Статус:");
			JComboBox statusField = new JComboBox();
			statusField.addItem("Заблокувати");
			statusField.addItem("Розблокувати");
			
			statusText.setLocation(455, 155);
			statusText.setSize(250, 25);
			panelForAddProgram.add(statusText);
			
			statusField.setLocation(455, 180);
			statusField.setSize(200, 20);
			panelForAddProgram.add(statusField);
			
						
			JButton createButton = new JButton("Створити");
			
			
			createButton.setLocation(675,115);
			createButton.setSize(100, 20);
			panelForAddProgram.add(createButton);
			
			
			JButton updateButton = new JButton("Оновити");
			
			
			updateButton.setLocation(675,115);
			updateButton.setSize(100, 20);
			panelForAddProgram.add(updateButton);
			
			
			
			
			
//******************************ELEMENTS**************************************************************			
//******************************Actions for JBox*******************************************************		
			roleField.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(roleField.getSelectedIndex() == 0) role = "адміністратор";
					else role = "менеджер";
					
				}
			});
			
			
			statusField.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(statusField.getSelectedIndex() == 0) block = "blocked";
					else block = "unblocked";
					
				}
			});
			
//******************************Actions for JBox*******************************************************				
//******************************Actions for Radio*******************************************************				
			
			createUser.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					createButton.setVisible(true);
					loginField1.setEditable(true);
					passwordText.setEnabled(false);
					passwordText.setSelected(true);
					passwordField.setVisible(true);
					loginField1.setVisible(true);
					
					if(createUser.isSelected()){
						loginField1.setText("");
	       				firstField.setText("");
	       				lastField.setText("");
	       				middleField.setText("");
	       				phoneField.setText("");
	       				mailField.setText("");   
	                }
					
				}
			});
			
			
			updateUser.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					createButton.setVisible(false);
					updateButton.setVisible(true);
					passwordText.setEnabled(true);
					passwordText.setSelected(false);
					passwordField.setVisible(false);
					loginField1.setEditable(false);
					
					loginField1.setText(String.valueOf(selectedUser.getLogin()));
					firstField.setText(String.valueOf(selectedUser.getFirstName()));
       				lastField.setText(String.valueOf(selectedUser.getLastName()));
       				middleField.setText(String.valueOf(selectedUser.getMiddleName()));
       				phoneField.setText(String.valueOf(selectedUser.getPhone()));
       				mailField.setText(String.valueOf(selectedUser.getMail()));
						
				}
			});
			
//******************************end Actions for Radio *******************************************************	
		createButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ConnectTo server22 = null;
				String newlogin = loginField1.getText().trim();
				String newPassword = passwordText.isSelected() ? passwordField.getText().trim() : password;
				String newRole =  role == null ? selectedUser.getRole() : role;;
				String newFirst = firstField.getText();
				String newLast = lastField.getText();
				String newMiddle = middleField.getText();
				String newPhone = phoneField.getText();
				String newMail = mailField.getText();
				String newStatus = block;
				String newGuarantee = "";
				int newIncome = 0;
				
				
				try {
					server22 = new ConnectTo();
				
				ObjectOutputStream outStream = new ObjectOutputStream(server22.clSocket.getOutputStream());
				ObjectInputStream inputStream = new ObjectInputStream(server22.clSocket.getInputStream());
				outStream.writeObject("create_user_admin");
				ObjectOutputStream outStreamObj = new ObjectOutputStream(server22.clSocket.getOutputStream());
				
				User newUser = new User(newlogin, newPassword, newRole, newFirst, newLast, newMiddle, newPhone, newMail, "", newIncome, newGuarantee, newStatus);
				 
				outStreamObj.writeObject(newUser); 
				String input = (String) inputStream.readObject();
				
				if("yes".equals(input)) 
					JOptionPane.showMessageDialog(null, "Новий користувач був успішно створений", "Повідомлення",
						JOptionPane.WARNING_MESSAGE); 
				else
				
					JOptionPane.showMessageDialog(null, "Користувач за таким логіном вже існує. Повторіть, будь ласка, спробу.", "Повідомлення",
							JOptionPane.WARNING_MESSAGE); 
				
				
				server22.clSocket.close();
					
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					
					e1.printStackTrace();
				}
				
			}
		});	
			
	//**********************************************************************************
		updateButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ConnectTo server22 = null;
				String newlogin = selectedUser.getLogin();
				String newPassword = passwordText.isSelected() ? passwordField.getText().trim() : password;
				String newRole = role == null ? selectedUser.getRole() : role;
				String newFirst = firstField.getText();
				String newLast = lastField.getText();
				String newMiddle = middleField.getText();
				String newPhone = phoneField.getText();
				String newMail = mailField.getText();
				String newStatus = block;
				String newGuarantee = selectedUser.getGuarantee();
				int newIncome = selectedUser.getIncome();
				
				
				try {
					server22 = new ConnectTo();
				
				ObjectOutputStream outStream = new ObjectOutputStream(server22.clSocket.getOutputStream());
				ObjectInputStream inputStream = new ObjectInputStream(server22.clSocket.getInputStream());
				outStream.writeObject("update_user_admin");
				ObjectOutputStream outStreamObj = new ObjectOutputStream(server22.clSocket.getOutputStream());
				
				User newUser = new User(newlogin, newPassword, newRole, newFirst, newLast, newMiddle, newPhone, newMail, "", newIncome, newGuarantee, newStatus);
				 
				outStreamObj.writeObject(newUser); 
				String input = (String) inputStream.readObject();
				
				if("yes".equals(input)) 
					JOptionPane.showMessageDialog(null, "Дані було успішно оновлено", "Повідомлення",
						JOptionPane.WARNING_MESSAGE); 
				else
				
					JOptionPane.showMessageDialog(null, "Відбулась помилка. Повторіть, будь ласка, спробу.", "Повідомлення",
							JOptionPane.WARNING_MESSAGE); 
				
				
				server22.clSocket.close();
					
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					
					e1.printStackTrace();
				}
				
			}
		});	
				

	//********************************Listen for selectors********************************************	
		
				ListSelectionModel selModel = tableForUser1.getSelectionModel();
				selModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);  
				selModel.addListSelectionListener(new ListSelectionListener() { 
						     
		               public void valueChanged(ListSelectionEvent e) {
		            	  
		            	   int selectRow = tableForUser1.getSelectedRow();
		                 
		                 try {
		                	 selectedUser = users.get(selectRow);
							} catch (Exception e2) {
							} 
		                    if(updateUser.isSelected()){
		                	password = selectedUser.getPassword();	       				
		       				loginField1.setText(selectedUser.getLogin());
		       				
		       				
		       				firstField.setText(String.valueOf(selectedUser.getFirstName()));
		       				lastField.setText(String.valueOf(selectedUser.getLastName()));
		       				middleField.setText(String.valueOf(selectedUser.getMiddleName()));
		       				phoneField.setText(String.valueOf(selectedUser.getPhone()));
		       				mailField.setText(String.valueOf(selectedUser.getMail()));
		       				
		                	   
		                   } 
		                	   
		                   } 
		                   
		               
		           		
		                             
		          });
		          

	//**********************************************************************************	
			
			
			
	}
}
