﻿package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;
import javax.swing.text.AbstractDocument;

import actions.OrderActionListener;
import serverconnnect.ConnectTo;
import vo.CreditProgram;
import vo.Order;
import vo.User;
import tables.TableForCreditProgramAdmin;
import tmodels.*;
import limits.*;

public class PanelForCreditProgramAdmin extends JPanel {
	private ArrayList<CreditProgram> creditPrograms;
	private ConnectTo server;
	private int maxSum;
	private int minSum;
	private int maxPeriod;
	private CreditProgram selectedCreditProgram;
	private String login;
	private OrderActionListener orderActionListener;
	private String block;
	private TableModel model;
	private JRadioButton updateProgram;
	private int idCreditProgram;
	
	private PanelForCreditProgramAdmin() {
		
	}


	public PanelForCreditProgramAdmin(String login) throws IOException {
				
//*****************************************************************************
		
		try {
			server = new ConnectTo();
			ObjectOutputStream outStream = new ObjectOutputStream(server.clSocket.getOutputStream());
			outStream.writeObject("credit_program_get");
			ObjectInputStream inStreamObject = new ObjectInputStream(server.clSocket.getInputStream());
			creditPrograms = (ArrayList<CreditProgram>) (inStreamObject.readObject());
	
	
//*****************************************************************************
		TableModel model = new ModelCreditProgramAdmin(creditPrograms);
		JTable tableForCreditProgram = new TableForCreditProgramAdmin(model);
		
		JScrollPane scrollPaneForCredit = new JScrollPane(tableForCreditProgram);
		scrollPaneForCredit.setPreferredSize(new Dimension(990, 180));
		scrollPaneForCredit.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPaneForCredit.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Кредитні програми")));
		tableForCreditProgram.setRowSelectionInterval(0, 0);
		add(scrollPaneForCredit);
		
//********************************************************************************	

				JPanel panelForFullDesc = new JPanel();
				panelForFullDesc.setLayout(new FlowLayout(FlowLayout.LEFT)); 
				panelForFullDesc.setPreferredSize(new Dimension(990, 90));
				panelForFullDesc.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Повний опис кредитної програми")));
				JTextArea TAreaForFullDesc = new JTextArea("Оберіть програму кредитування з таблиці.", 2, 86);
				TAreaForFullDesc.setLineWrap(isEnabled());
				TAreaForFullDesc.setBackground(panelForFullDesc.getBackground());
				TAreaForFullDesc.setWrapStyleWord(true);
				TAreaForFullDesc.setEditable(false);
				panelForFullDesc.add(TAreaForFullDesc);
				add(panelForFullDesc);

							

	

		JPanel panelForAddProgram = new JPanel();
		panelForAddProgram.setLayout(null); 
		panelForAddProgram.setPreferredSize(new Dimension(990, 200));
		panelForAddProgram.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Додати/оновити "
				+ "кредитну програму")));
		
		add(panelForAddProgram);
		
		
		
		JRadioButton createProgram   = new JRadioButton("Створити нову кредитну програму");
		createProgram.setSelected(true);
		updateProgram    = new JRadioButton("Оновити поточну кредитну програму");
		

		ButtonGroup bgroup = new ButtonGroup();
		bgroup.add(createProgram);
		bgroup.add(updateProgram);
		

		createProgram.setLocation(350,10);
		createProgram.setSize(240, 25);
		
		panelForAddProgram.add(createProgram);
		
		updateProgram.setLocation(650,10);
		updateProgram.setSize(260, 20);
		
		panelForAddProgram.add(updateProgram);
		

		
		JLabel nameText = new JLabel("Назва:");
		JTextField nameField = new JTextField();
		
		nameText.setLocation(15,35);
		nameText.setSize(250, 25);
		panelForAddProgram.add(nameText);
		
		nameField.setLocation(15, 60);
		nameField.setSize(200, 20);
		nameField.setDocument(new JTextFieldLimit(30));
		panelForAddProgram.add(nameField);
		
		
		JLabel periodText = new JLabel("Період:");
		JTextField periodField = new JTextField();
		
		periodText.setLocation(235,35);
		periodText.setSize(250, 25);
		panelForAddProgram.add(periodText);
		
		periodField.setLocation(235, 60);
		periodField.setSize(35, 20);
		periodField.setDocument(new JTextFieldLimit(3));
		panelForAddProgram.add(periodField);
		
		
		JLabel maxSumText = new JLabel("Макс. сума:");
		JTextField maxSumField = new JTextField();
		
		maxSumText.setLocation(135,85);
		maxSumText.setSize(250, 25);
		panelForAddProgram.add(maxSumText);
		
		maxSumField.setLocation(135, 110);
		maxSumField.setSize(80, 20);
		maxSumField.setDocument(new JTextFieldLimit(8));
		panelForAddProgram.add(maxSumField);
		
		
		JLabel minSumText = new JLabel("Мін. сума:");
		JTextField minSumField = new JTextField();
		
		minSumText.setLocation(15,85);
		minSumText.setSize(250, 25);
		panelForAddProgram.add(minSumText);
		
		minSumField.setLocation(15, 110);
		minSumField.setSize(80, 20);
		minSumField.setDocument(new JTextFieldLimit(8));
		panelForAddProgram.add(minSumField);
		
		
		JLabel percentText = new JLabel("Процент:");
		JTextField percentField = new JTextField();
		
		percentText.setLocation(290,35);
		percentText.setSize(250, 25);
		panelForAddProgram.add(percentText);
		
		percentField.setLocation(290, 60);
		percentField.setSize(35, 20);
		percentField.setDocument(new JTextFieldLimit(8));
		panelForAddProgram.add(percentField);
		
		
		JLabel ShortText = new JLabel("Короткий опис:");
		JTextArea ShortField = new JTextArea();
		ShortField.setDocument(new JTextFieldLimit(55));
				
		ShortText.setLocation(450,35);
		ShortText.setSize(200, 25);
		panelForAddProgram.add(ShortText);
		
		ShortField.setLocation(450, 60);
		ShortField.setSize(400, 20);
		ShortField.setLineWrap(true);
		ShortField.setWrapStyleWord(true);
		panelForAddProgram.add(ShortField);
		
		JLabel FullText = new JLabel("Повний опис:");
		JTextArea FullField = new JTextArea();
		
		FullText.setLocation(450, 85);
		FullText.setSize(200, 25);
		panelForAddProgram.add(FullText);
		
		FullField.setLocation(450, 110);
		FullField.setSize(500, 80);
		FullField.setDocument(new JTextFieldLimit(475));
		FullField.setLineWrap(true);
		panelForAddProgram.add(FullField);
		
		
		JButton createButton = new JButton("Створити");
		
		
		createButton.setLocation(120,150);
		createButton.setSize(100, 30);
		panelForAddProgram.add(createButton);
		
		
		JButton updateButton = new JButton("Оновити");
		
		
		updateButton.setLocation(120,150);
		updateButton.setSize(100, 30);
		panelForAddProgram.add(updateButton);
		
		JLabel blockText = new JLabel("Статус");
		blockText.setLocation(225, 85);
		blockText.setSize(100, 25);
		panelForAddProgram.add(blockText);
		
		JComboBox faceCombo = new JComboBox();
		faceCombo.addItem("Заблокувати");
		faceCombo.addItem("Розблокувати");
		faceCombo.setLocation(225, 110);
		faceCombo.setSize(120, 18);
		panelForAddProgram.add(faceCombo);
		
		
		
		faceCombo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(faceCombo.getSelectedIndex() == 0) block = "blocked";
				else block = "unblocked";
				
			}
		});
		
		
		
		
		createProgram.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				createButton.setVisible(true);
				updateButton.setVisible(false);
				if(createProgram.isSelected()){
             	   nameField.setText("");
	       				ShortField.setText("");
	       				FullField.setText("");
	       				periodField.setText("");
	       				percentField.setText("");
	       				minSumField.setText("");
	       				maxSumField.setText("");
                }
				
			}
		});
		
		
		updateProgram.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				createButton.setVisible(false);
				updateButton.setVisible(true);
				Map<Integer, CreditProgram> creditProgramsMap = ModelCreditProgramAdmin.getCreditProgramsMap();
				int selectRow = tableForCreditProgram.getSelectedRow();
				idCreditProgram = creditPrograms.get(selectRow).getIdCreditProgram();
				CreditProgram  SelectedCreditProgram = creditProgramsMap.get(idCreditProgram);
				nameField.setText(SelectedCreditProgram.getName());
				ShortField.setText(SelectedCreditProgram.getShortDescription());
				FullField.setText(TAreaForFullDesc.getText());
				periodField.setText(String.valueOf(SelectedCreditProgram.getMaxPeriod()));
				percentField.setText(String.valueOf(SelectedCreditProgram.getPercent()*100));
				System.out.println("Procent="+SelectedCreditProgram.getPercent()*100);
				minSumField.setText(String.valueOf(SelectedCreditProgram.getMinMoneyAmoun()));
				maxSumField.setText(String.valueOf(SelectedCreditProgram.getMaxMoneyAmount()));
			}
		});
		
//**********************************************************************************
	createButton.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ConnectTo server22 = null;
			String name = nameField.getText();
			String shortDesc = ShortField.getText();
			String fullDesc = FullField.getText();
			int maxPeriod = Integer.valueOf(periodField.getText());
			int maxMoneyAmount = Integer.valueOf(maxSumField.getText());
			int minMoneyAmoun = Integer.valueOf(minSumField.getText());
			float percent = Float.valueOf(percentField.getText())/100;
			
			try {
				server22 = new ConnectTo();
			
			ObjectOutputStream outStream = new ObjectOutputStream(server22.clSocket.getOutputStream());
			ObjectInputStream inputStream = new ObjectInputStream(server22.clSocket.getInputStream());
			outStream.writeObject("create_program");
			ObjectOutputStream outStreamObj = new ObjectOutputStream(server22.clSocket.getOutputStream());
			
			CreditProgram newCreditProgram = new CreditProgram(0, name, shortDesc, fullDesc, maxPeriod, 
					maxMoneyAmount,  percent, minMoneyAmoun, block);
			 
			outStreamObj.writeObject(newCreditProgram); 
			String input = (String) inputStream.readObject();
			
			if("yes".equals(input)) 
				JOptionPane.showMessageDialog(null, "Дані було успішно добавлено", "Повідомлення",
					JOptionPane.WARNING_MESSAGE); 
			else
			
				JOptionPane.showMessageDialog(null, "Відбулась помилка. Повторіть, будь ласка, спробу.", "Повідомлення",
						JOptionPane.WARNING_MESSAGE); 
			
			server22.clSocket.close();
				
			} catch (UnknownHostException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (ClassNotFoundException e1) {
				
				e1.printStackTrace();
			}
			
		}
	});	
		
//**********************************************************************************
	updateButton.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ConnectTo server22 = null;
			String name = nameField.getText();
			String shortDesc = ShortField.getText();
			String fullDesc = FullField.getText();
			int maxPeriod = Integer.valueOf(periodField.getText());
			int maxMoneyAmount = Integer.valueOf(maxSumField.getText());
			int minMoneyAmoun = Integer.valueOf(minSumField.getText());
			float percent = Float.valueOf(percentField.getText())/100;
			
			try {
				server22 = new ConnectTo();
			
			ObjectOutputStream outStream = new ObjectOutputStream(server22.clSocket.getOutputStream());
			ObjectInputStream inputStream = new ObjectInputStream(server22.clSocket.getInputStream());
			outStream.writeObject("update_program");
			ObjectOutputStream outStreamObj = new ObjectOutputStream(server22.clSocket.getOutputStream());
			
			CreditProgram newCreditProgram = new CreditProgram(idCreditProgram, name, shortDesc, fullDesc, maxPeriod, 
					maxMoneyAmount,  percent, minMoneyAmoun, block);
			 
			outStreamObj.writeObject(newCreditProgram); 
			String input = (String) inputStream.readObject();
			
			if("yes".equals(input)) 
				JOptionPane.showMessageDialog(null, "Дані було успішно оновлено", "Повідомлення",
					JOptionPane.WARNING_MESSAGE); 
			else
			
				JOptionPane.showMessageDialog(null, "Відбулась помилка. Повторіть, будь ласка, спробу.", "Повідомлення",
						JOptionPane.WARNING_MESSAGE); 
			
			server22.clSocket.close();
				
			} catch (UnknownHostException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (ClassNotFoundException e1) {
				
				e1.printStackTrace();
			}
			
		}
	});	
			

//**********************************************************************************	
	
			ListSelectionModel selModel = tableForCreditProgram.getSelectionModel();
			selModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);  
			selModel.addListSelectionListener(new ListSelectionListener() { 
					     
	               public void valueChanged(ListSelectionEvent e) {
	                   int selectRow = tableForCreditProgram.getSelectedRow();
	                   TAreaForFullDesc.setText(creditPrograms.get(selectRow).getFullDescription());
	                   selectedCreditProgram = creditPrograms.get(selectRow);
	                   if(updateProgram.isSelected()){
	                	Map<Integer, CreditProgram> creditProgramsMap = ModelCreditProgramAdmin.getCreditProgramsMap();
	       			    idCreditProgram = creditPrograms.get(selectRow).getIdCreditProgram();
	       				CreditProgram  SelectedCreditProgram = creditProgramsMap.get(idCreditProgram);
	       				nameField.setText(SelectedCreditProgram.getName());
	       				ShortField.setText(SelectedCreditProgram.getShortDescription());
	       				FullField.setText(TAreaForFullDesc.getText());
	       				periodField.setText(String.valueOf(SelectedCreditProgram.getMaxPeriod()));
	       				percentField.setText(String.valueOf(SelectedCreditProgram.getPercent()*100));
	       				minSumField.setText(String.valueOf(SelectedCreditProgram.getMinMoneyAmoun()));
	       				maxSumField.setText(String.valueOf(SelectedCreditProgram.getMaxMoneyAmount()));
	                	   
	                   } 
	                   
	               
	           		
	               }               
	          });
	          

//**********************************************************************************			
		
		} catch (UnknownHostException e) {
			e.printStackTrace();
	} catch (IOException e) {
			e.printStackTrace();
	} catch (ClassNotFoundException e) {
			e.printStackTrace();
	}
		finally{
			server.clSocket.close();
		}	
		
	}
 
}
