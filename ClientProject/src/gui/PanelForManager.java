﻿package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableModel;

import actions.UserChangesActionListener;
import serverconnnect.ConnectTo;
import tables.TableForCreditProgramAdmin;
import tables.TableForOrders;
import tables.TableForUser;
import tmodels.ModelCreditProgramAdmin;
import tmodels.ModelCreditUnit;
import tmodels.ModelOrder;
import tmodels.ModelOrderManager;
import tmodels.ModelUser1;
import tmodels.ModelUser2;
import tmodels.ModelUserAdmin;
import vo.CreditProgram;
import vo.CreditUnit;
import vo.Order;
import vo.User;

public class PanelForManager extends JPanel {
	private String login;
	private User user;
	private List<Order> orders;
	private List<Order> orders1;
	private Map<Integer, CreditProgram> creditProgramsMap;
	private Map<String, User> usersNoPassword;
	private JTabbedPane tabPane;
	private ArrayList<CreditUnit> creditUnits;
	
	private PanelForManager(){
		
			}
	
	public PanelForManager(String login, JTabbedPane tabPane) throws IOException{
	//*****************************************************************************
		this.login = login;
		this.tabPane = tabPane;
		
	ConnectTo server = null;
			try {
				server = new ConnectTo();
				ObjectOutputStream outStream = new ObjectOutputStream(server.clSocket.getOutputStream());
				outStream.writeObject("orders_get");
				outStream.writeObject(login);
				
				ObjectInputStream inStreamObject1 = new ObjectInputStream(server.clSocket.getInputStream());
				orders = (ArrayList<Order>) (inStreamObject1.readObject());
				orders1 = (ArrayList<Order>) (inStreamObject1.readObject());
				
		
		} catch (UnknownHostException e) {
				e.printStackTrace();
		} catch (IOException e) {
				e.printStackTrace();
		} catch (ClassNotFoundException e) {
				e.printStackTrace();
		}
			finally{
				server.clSocket.close();
			}
			
	//*****************************************************************************
		
			creditProgramsMap = ModelCreditProgramAdmin.getCreditProgramsMap();
			TableModel modelOrders = new ModelOrderManager(orders, creditProgramsMap, usersNoPassword);
			
			JPanel panel1 = new JPanel();
			panel1.setPreferredSize(new Dimension(990, 150));
			panel1.setLayout(new BorderLayout());
			panel1.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Черга поданих заявок на кредит")));
			add(panel1);
			JTable tableForOrder = new TableForOrders(modelOrders);
			
			//tableForOrder.setRowSelectionInterval(0, 0);
			JScrollPane scrollPaneForOrder = new JScrollPane(tableForOrder);
			scrollPaneForOrder.setPreferredSize(new Dimension(990, 90));
			scrollPaneForOrder.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
						
			panel1.add(scrollPaneForOrder, BorderLayout.NORTH );
			JPanel panel3 = new JPanel();
			panel3.setLayout(null);
			panel3.setSize(990, 100);
			panel1.add(panel3);
			
			JButton takeButton = new JButton("Обробити заявку");
			takeButton.setLocation(400, 10);
			takeButton.setSize(190, 20);
			panel3.add(takeButton);
//********************************************************************************	
takeButton.addActionListener(new ActionListener() {
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int index = tableForOrder.getSelectedRow();
		
		Order order = orders.get(index);
				
		ConnectTo server1 = null;
		Order newOrder = new Order(order.getIdOrder(), order.getIdCreditProgram(), "В огляді", login, order.getDateCreation(),
				order.getDurationCredit(), order.getSumCredit(), order.getLoginClient());
		
		try {
			server1 = new ConnectTo();
			ObjectOutputStream outStream = new ObjectOutputStream(server1.clSocket.getOutputStream());
			outStream.writeObject("orders_update");
			
			ObjectOutputStream outStream1 = new ObjectOutputStream(server1.clSocket.getOutputStream());
			outStream1.writeObject(newOrder);
			ObjectInputStream inStream = new ObjectInputStream(server1.clSocket.getInputStream());
			String answer = (String) inStream.readObject();
			if("yes".equals(answer)) {
			JOptionPane.showMessageDialog(null, "Заява  було успішно оброблена", "Повідомлення" ,
					JOptionPane.INFORMATION_MESSAGE);
			}
			
			
			server1.clSocket.close();
	//***************************************************************
			Component selected = tabPane.getSelectedComponent();
			tabPane.addTab("Управління заявками на кредити", new PanelForManager(login, tabPane));
			tabPane.remove(selected);
	//***************************************************************	
			
	
	} catch (UnknownHostException e1) {
			e1.printStackTrace();
	} catch (IOException e1) {
			e1.printStackTrace();
	} catch (ClassNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
		
		
		
	}
});
			

//*********************************************************************************************
			creditProgramsMap = ModelCreditProgramAdmin.getCreditProgramsMap();
			TableModel modelOrders1 = new ModelOrderManager(orders1, creditProgramsMap, usersNoPassword);
			
			JPanel panel2 = new JPanel();
			panel2.setPreferredSize(new Dimension(990, 160));
			panel2.setLayout(new BorderLayout());
			panel2.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Заявки, які ви оброблюєте:")));
			add(panel2);
			JTable tableForOrder1 = new TableForOrders(modelOrders1);
			JScrollPane scrollPaneForOrder1 = new JScrollPane(tableForOrder1);
			scrollPaneForOrder1.setPreferredSize(new Dimension(990, 100));
			scrollPaneForOrder1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
						
			panel2.add(scrollPaneForOrder1, BorderLayout.NORTH );
			JPanel panel4 = new JPanel();
			panel4.setLayout(null);
			panel4.setSize(990, 60);
			panel2.add(panel4);
			
			JButton takeButton1 = new JButton("Оформити кредит");
			takeButton1.setLocation(250, 10);
			takeButton1.setSize(190, 20);
			panel4.add(takeButton1);
			
			JButton takeButton2 = new JButton("Відхилити кредит");
			takeButton2.setLocation(550, 10);
			takeButton2.setSize(190, 20);
			panel4.add(takeButton2);
			
			takeButton1.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					int index = tableForOrder1.getSelectedRow();
					System.out.println("index="+index);
					Order order = orders1.get(index);
							
					ConnectTo server1 = null;
				Order newOrder = new Order(order.getIdOrder(), order.getIdCreditProgram(), "Оброблена", login, order.getDateCreation(),
							order.getDurationCredit(), order.getSumCredit(), order.getLoginClient());
				CreditUnit creditUnit = new CreditUnit(0, order.getIdOrder(), "", String.valueOf(order.getDurationCredit()), "В оплаті", order.getLoginClient());
				
					try {
						server1 = new ConnectTo();
						ObjectOutputStream outStream = new ObjectOutputStream(server1.clSocket.getOutputStream());
						outStream.writeObject("credit_unit_create");
						
						outStream = new ObjectOutputStream(server1.clSocket.getOutputStream());
						outStream.writeObject(newOrder);
						System.out.println("IdOrder="+newOrder.getIdOrder());
						outStream = new ObjectOutputStream(server1.clSocket.getOutputStream());
						outStream.writeObject(creditUnit);
						ObjectInputStream inStream = new ObjectInputStream(server1.clSocket.getInputStream());
						String answer = (String) inStream.readObject();
						System.out.println(answer);
						if("yes".equals(answer)) {
							JOptionPane.showMessageDialog(null, "Кредит був успішно оформлений", "Повідомлення" ,
									JOptionPane.INFORMATION_MESSAGE);
							}
						server1.clSocket.close();
				
				//***************************************************************
						Component selected = tabPane.getSelectedComponent();
						tabPane.addTab("Управління заявками на кредити", new PanelForManager(login, tabPane));
						tabPane.remove(selected);
				//***************************************************************		
						
						
				
				} catch (UnknownHostException e1) {
						e1.printStackTrace();
				} catch (IOException e1) {
						e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
					
					
				}
			});
			
//********************************************************************************	
takeButton2.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					int index = tableForOrder1.getSelectedRow();
					System.out.println("index="+index);
					Order order = orders1.get(index);
							
					ConnectTo server1 = null;
				Order newOrder = new Order(order.getIdOrder(), order.getIdCreditProgram(), "Відхилена", login, order.getDateCreation(),
							order.getDurationCredit(), order.getSumCredit(), order.getLoginClient());
							
					try {
						server1 = new ConnectTo();
						ObjectOutputStream outStream = new ObjectOutputStream(server1.clSocket.getOutputStream());
						outStream.writeObject("orders_update");
						
						outStream = new ObjectOutputStream(server1.clSocket.getOutputStream());
						outStream.writeObject(newOrder);
												
						ObjectInputStream inStream = new ObjectInputStream(server1.clSocket.getInputStream());
						String answer = (String) inStream.readObject();
						System.out.println(answer);
						if("yes".equals(answer)) {
							JOptionPane.showMessageDialog(null, "Кредит був відхилений", "Повідомлення" ,
									JOptionPane.INFORMATION_MESSAGE);
							}
						server1.clSocket.close();
				
					
				//***************************************************************
						Component selected = tabPane.getSelectedComponent();
						tabPane.addTab("Управління заявками на кредити", new PanelForManager(login, tabPane));
						tabPane.remove(selected);
				//***************************************************************								
						
				
				} catch (UnknownHostException e1) {
						e1.printStackTrace();
				} catch (IOException e1) {
						e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
					
					
				}
			});
		
//**************************************************************************************************			
			JPanel panelForInfoChange = new JPanel();
			panelForInfoChange.setPreferredSize(new Dimension(990, 160));
			panelForInfoChange.setLayout(new BorderLayout());
			panelForInfoChange.setBorder(new CompoundBorder(new EmptyBorder(0, 0, 0, 0), new TitledBorder("Оформлені кредити на поточного клієнта")));
			add(panelForInfoChange);
			ArrayList<CreditUnit> creditUnitsTemp = new ArrayList<CreditUnit>();
			creditUnitsTemp.add(new CreditUnit(0,0,"", "", "",""));
			JTable tableForOrder6 = new JTable(new ModelCreditUnit(creditUnitsTemp));
			JScrollPane scrollPaneForOrder6 = new JScrollPane(tableForOrder6);
			scrollPaneForOrder6.setPreferredSize(new Dimension(990, 70));
			scrollPaneForOrder6.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			panelForInfoChange.add(scrollPaneForOrder6, BorderLayout.NORTH );
			
			JPanel panel6 = new JPanel();
			panel6.setLayout(null);
			panel6.setSize(990, 10);
			panelForInfoChange.add(panel6);
			
			JButton takeButton6 = new JButton("Знайти");
			takeButton6.setLocation(225, 30);
			takeButton6.setSize(190, 20);
			panel6.add(takeButton6);
			
			JLabel label1 = new JLabel("Логін користувача:");
			label1.setLocation(15, 5);
			label1.setSize(190, 20);
			panel6.add(label1);
			
			JTextField loginField = new JTextField();
			loginField.setLocation(15, 30);
			loginField.setSize(190, 20);
			panel6.add(loginField);
			
			
			JLabel StatusText = new JLabel("Статус кредиту:");
			StatusText.setLocation(550,5);
			StatusText.setSize(150, 20);
			panel6.add(StatusText);
			
			JComboBox StatusField = new JComboBox();
			StatusField.addItem("В оплаті");
			StatusField.addItem("Просрочений");
			StatusField.addItem("Виплачений");
			StatusField.addItem("Заблокований");
			StatusField.setLocation(550, 30);
			StatusField.setSize(150, 20);
			panel6.add(StatusField);
			
			
			JButton takeButton7 = new JButton("Змінити статус");
			takeButton7.setLocation(725, 30);
			takeButton7.setSize(150, 20);
			panel6.add(takeButton7);
//********************************************************************************
takeButton7.addActionListener(new ActionListener() {
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int index = tableForOrder6.getSelectedRow();
		CreditUnit creditUnit = creditUnits.get(index);
		String status = (String) StatusField.getSelectedItem();
				
		CreditUnit newCreditUnit = new CreditUnit (creditUnit.getIdCredit(), creditUnit.getIdOrder(), creditUnit.getDateStart(),
				creditUnit.getDateEnd(), status, creditUnit.getLogin());
		ConnectTo server = null;
		try {
			server = new ConnectTo();
			ObjectOutputStream outStream = new ObjectOutputStream(server.clSocket.getOutputStream());
			outStream.writeObject("update_credit_unit");
			outStream = new ObjectOutputStream(server.clSocket.getOutputStream());
			outStream.writeObject(newCreditUnit);
			ObjectInputStream inStream = new ObjectInputStream(server.clSocket.getInputStream());
												
			String s = (String) inStream.readObject();
			if("yes".equals(s)) {
				JOptionPane.showMessageDialog(null, "Статус було змінено успішно", "Повідомлення" ,
						JOptionPane.INFORMATION_MESSAGE);
				}
			server.clSocket.close();
	
	//***************************************************************
			Component selected = tabPane.getSelectedComponent();
			tabPane.addTab("Управління заявками на кредити", new PanelForManager(login, tabPane));
			tabPane.remove(selected);
	//***************************************************************					
			
	} catch (UnknownHostException e1) {
			e1.printStackTrace();
	} catch (IOException e1) {
			e1.printStackTrace();
	} catch (ClassNotFoundException e1) {
		e1.printStackTrace();
	}
		
	}
});			
			
			
			
//********************************************************************************				
			takeButton6.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					String login = loginField.getText().trim();
					ConnectTo server = null;
					try {
						server = new ConnectTo();
						ObjectOutputStream outStream = new ObjectOutputStream(server.clSocket.getOutputStream());
						outStream.writeObject("find_credit_unit");
						outStream.writeObject(login);
						
						ObjectInputStream inStream = new ObjectInputStream(server.clSocket.getInputStream());
						
						ObjectInputStream inStreamObj = new ObjectInputStream(server.clSocket.getInputStream());
						creditUnits = (ArrayList<CreditUnit>) (inStreamObj.readObject());
													
						String s = (String) inStream.readObject();
						
						server.clSocket.close();
				
				} catch (UnknownHostException e1) {
						e1.printStackTrace();
				} catch (IOException e1) {
						e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
					tableForOrder6.setModel(new ModelCreditUnit(creditUnits));	
					
				}
			});		
//**********************************************************************			
	}
}
