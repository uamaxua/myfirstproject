﻿package dao;

import java.sql.SQLException;

public interface Dao<VO> {
	void creatEntity(VO entity) throws SQLException;
	void updateEntity(VO entity) throws SQLException;
	VO readEntity(int id, String login) throws SQLException;
	void deleteEntity(int id);
	
}
