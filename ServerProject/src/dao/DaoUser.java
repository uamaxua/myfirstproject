﻿package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import vo.CreditProgram;
import vo.User;

public class DaoUser implements Dao<User> {

	
	
	public void creatEntity(User entity) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "insert into uamaxua.bank_user "
        		+ "(LOGIN, PASSWORD, ROLE, FIRST_NAME, LAST_NAME, MIDDLE_NAME, PHONE, MAIL, CREATE_DATE, INCOME, GUARANTEE, STATUS) "
        		+ "values (?, ?, ?, ?, ?, ?, ?, ?, sysdate, ?, ?, ?)" );
        System.out.println("login="+entity.getLogin());
        preparedStatement.setString( 1, entity.getLogin() );
        preparedStatement.setString( 2, entity.getPassword() );
        preparedStatement.setString( 3, entity.getRole() );
        preparedStatement.setString( 4, entity.getFirstName() );
        preparedStatement.setString( 5, entity.getLastName());
        preparedStatement.setString( 6, entity.getMiddleName() );
        preparedStatement.setString( 7, entity.getPhone() );
        preparedStatement.setString( 8, entity.getMail());
        preparedStatement.setInt( 9, entity.getIncome() );
        preparedStatement.setString( 10, entity.getGuarantee() );
        preparedStatement.setString( 11, entity.getStatus() );
        preparedStatement.executeUpdate();
		
	}

	public void updateEntity(User entity) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "UPDATE uamaxua.bank_user SET PASSWORD = ?, FIRST_NAME = ?,"
        		+ "LAST_NAME = ?, MIDDLE_NAME = ?, PHONE =?, MAIL= ?, INCOME = ?, GUARANTEE = ?, ROLE = ?, STATUS =?  WHERE LOGIN = ?" );
        System.out.println("login="+entity.getLogin());
        preparedStatement.setString( 1, entity.getPassword() );
        preparedStatement.setString( 2, entity.getFirstName() );
        preparedStatement.setString( 3, entity.getLastName() );
        preparedStatement.setString( 4, entity.getMiddleName() );
        preparedStatement.setString( 5, entity.getPhone() );
        preparedStatement.setString( 6, entity.getMail() );
        preparedStatement.setInt( 7, entity.getIncome() );
        preparedStatement.setString( 8, entity.getGuarantee() );
        preparedStatement.setString( 9, entity.getRole() );
        preparedStatement.setString( 10, entity.getStatus() );
        preparedStatement.setString( 11, entity.getLogin() );
        preparedStatement.executeUpdate();
		
	}

	public User readEntity(int id, String login) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_user  WHERE login = ?" );
        preparedStatement.setString( 1, login);
        ResultSet resultSet = preparedStatement.executeQuery();
		User user = null;
        if ( resultSet.next() ) {
        	user = new User(resultSet.getString("LOGIN"), resultSet.getString("PASSWORD"), resultSet.getString("ROLE"), 
        			resultSet.getString("FIRST_NAME"), resultSet.getString("LAST_NAME"), resultSet.getString("MIDDLE_NAME"), 
        			resultSet.getString("PHONE"), resultSet.getString("MAIL"), resultSet.getString("CREATE_DATE"),
        			resultSet.getInt("INCOME"), resultSet.getString("GUARANTEE"), resultSet.getString("STATUS"));
        	       	
        }
        resultSet.close();
        connection.close();
        return user;
	}

	public void deleteEntity(int id) {
		// TODO Auto-generated method stub
		
	}
	
	public Map<String, User> safeReadAll() throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_user" );
        ResultSet resultSet = preparedStatement.executeQuery();
        Map<String, User> users = new HashMap<String, User>();
        User temp = null;
        while ( resultSet.next() ) {
        	temp = new User(resultSet.getString("LOGIN"), "********", resultSet.getString("ROLE"), 
        			resultSet.getString("FIRST_NAME"), resultSet.getString("LAST_NAME"), resultSet.getString("MIDDLE_NAME"), 
        			resultSet.getString("PHONE"), resultSet.getString("MAIL"), resultSet.getString("CREATE_DATE"),
        			resultSet.getInt("INCOME"), resultSet.getString("GUARANTEE"), resultSet.getString("STATUS"));
        	users.put(resultSet.getString("LOGIN"), temp );
        	       	
        }
        resultSet.close();
        connection.close();
        return users;
	}
	
	public ArrayList<User> findEntity(String login, String firstName, String lastName) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_user WHERE LOGIN LIKE ? "
        		+ "AND LAST_NAME LIKE ? AND FIRST_NAME LIKE ?" );
        preparedStatement.setString( 1, login+"%");
        preparedStatement.setString( 2, lastName+"%");
        preparedStatement.setString( 3, firstName+"%");
        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<User> users = new ArrayList<User>();
		User temp = null;
		while ( resultSet.next() ) {
        	temp = new User(resultSet.getString("LOGIN"), resultSet.getString("PASSWORD"), resultSet.getString("ROLE"), 
        			resultSet.getString("FIRST_NAME"), resultSet.getString("LAST_NAME"), resultSet.getString("MIDDLE_NAME"), 
        			resultSet.getString("PHONE"), resultSet.getString("MAIL"), resultSet.getString("CREATE_DATE"),
        			resultSet.getInt("INCOME"), resultSet.getString("GUARANTEE"), resultSet.getString("STATUS"));
        	users.add(temp);
        	       	
        }
        resultSet.close();
        connection.close();
        return users;
	}
	
	public ArrayList<User> findEntityManager(String login, String firstName, String lastName) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_user WHERE LOGIN LIKE ? "
        		+ "AND LAST_NAME LIKE ? AND FIRST_NAME LIKE ? AND ROLE LIKE 'клієнт' " );
        preparedStatement.setString( 1, login+"%");
        preparedStatement.setString( 2, lastName+"%");
        preparedStatement.setString( 3, firstName+"%");
        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<User> users = new ArrayList<User>();
		User temp = null;
		while ( resultSet.next() ) {
        	temp = new User(resultSet.getString("LOGIN"), resultSet.getString("PASSWORD"), resultSet.getString("ROLE"), 
        			resultSet.getString("FIRST_NAME"), resultSet.getString("LAST_NAME"), resultSet.getString("MIDDLE_NAME"), 
        			resultSet.getString("PHONE"), resultSet.getString("MAIL"), resultSet.getString("CREATE_DATE"),
        			resultSet.getInt("INCOME"), resultSet.getString("GUARANTEE"), resultSet.getString("STATUS"));
        	users.add(temp);
        	       	
        }
        resultSet.close();
        connection.close();
        return users;
	}
	
	
	
	
}
