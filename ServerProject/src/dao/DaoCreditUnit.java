﻿package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vo.CreditUnit;
import vo.Order;
import vo.User;

public class DaoCreditUnit implements Dao<CreditUnit> {

	public void creatEntity(CreditUnit entity) throws SQLException {
		System.out.println("create Credit Unit");
		Connection connection = OracleConnect.getOracleConnect();
		PreparedStatement preparedStatement = connection.prepareStatement( "insert into uamaxua.bank_credit_unit (ID_CREDIT, "
				+ "ID_ORDER, DATE_START, DATE_END, STATUS, LOGIN) "
        		+ "values (uamaxua.INCREMENT_FOR_CREDITUNIT.nextval, ?, sysdate, ADD_MONTHS(sysdate, ?), ?, ?)" );
        System.out.println("preparedStatement created");
        preparedStatement.setInt( 1, entity.getIdOrder() );
        preparedStatement.setString( 2, entity.getDateEnd() );
        preparedStatement.setString( 3, entity.getStatus() );
        preparedStatement.setString( 4, entity.getLogin());
       
        
     
        preparedStatement.executeUpdate();
		
	}

	public void updateEntity(CreditUnit entity) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
		
        PreparedStatement preparedStatement = connection.prepareStatement( "UPDATE uamaxua.bank_credit_unit SET "
        		+ "STATUS = ? WHERE ID_CREDIT = ?" );
        System.out.println("preparedStatement created");
        preparedStatement.setString( 1, entity.getStatus() );
        preparedStatement.setInt( 2, entity.getIdCredit() );
        
        	preparedStatement.executeUpdate();
		
	}

	public ArrayList<CreditUnit> readEntityByLogin(int id, String login) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_credit_unit WHERE login = ?" );
        preparedStatement.setString( 1, login );
        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<CreditUnit> creditUnits = new ArrayList<CreditUnit>();
        CreditUnit temp = null;
        while ( resultSet.next() ) {
        	temp= new CreditUnit(resultSet.getInt( "ID_CREDIT" ), resultSet.getInt( "ID_ORDER" ), resultSet.getString( "DATE_START" ),
        			resultSet.getString( "DATE_END" ), resultSet.getString( "STATUS" ), resultSet.getString( "LOGIN" ));
        			creditUnits.add(temp);
         }
        resultSet.close();
        connection.close();
        return creditUnits;
	}

	public void deleteEntity(int id) {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<CreditUnit> readAll() throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_credit_unit" );
       
        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<CreditUnit> creditUnits = new ArrayList<CreditUnit>();
        CreditUnit temp = null;
        while ( resultSet.next() ) {
        	temp= new CreditUnit(resultSet.getInt( "ID_CREDIT" ), resultSet.getInt( "ID_ORDER" ), resultSet.getString( "DATE_START" ),
        			resultSet.getString( "DATE_END" ), resultSet.getString( "STATUS" ), resultSet.getString( "LOGIN" ));
        	creditUnits.add(temp);
         }
        resultSet.close();
        connection.close();
        return creditUnits;
	}	

	public ArrayList<CreditUnit> findEntityLogin(String login) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_credit_unit WHERE LOGIN LIKE ? ");
        preparedStatement.setString( 1, login+"%");
        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<CreditUnit> creditUnits = new ArrayList<CreditUnit>();
		CreditUnit temp = null;
		while ( resultSet.next() ) {
			temp= new CreditUnit(resultSet.getInt( "ID_CREDIT" ), resultSet.getInt( "ID_ORDER" ), resultSet.getString( "DATE_START" ),
        			resultSet.getString( "DATE_END" ), resultSet.getString( "STATUS" ), resultSet.getString( "LOGIN" ));
			creditUnits.add(temp);
        	       	
        }
        resultSet.close();
        connection.close();
        return creditUnits;
	}

	public CreditUnit readEntity(int id, String login) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	

}
