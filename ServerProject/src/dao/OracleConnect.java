﻿package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class OracleConnect{
	
	private final static String jdbcURL = "jdbc:oracle:thin:@localhost:1522:XE";
	private final static String oracleDriver = "oracle.jdbc.driver.OracleDriver";
	private final static String login = "system";
	private final static String password = "12345";
	
	public static  Connection  getOracleConnect() throws SQLException {
		System.setProperty("jdbc.drivers", oracleDriver);
		return DriverManager.getConnection(jdbcURL, login, password);
		}

}

