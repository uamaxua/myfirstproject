﻿package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import vo.CreditProgram;
public class DaoCreditProgram implements Dao<CreditProgram> {

	public void creatEntity(CreditProgram entity) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
		PreparedStatement preparedStatement = connection.prepareStatement( "insert into uamaxua.bank_credit_program (ID_CREDIT_PROGRAM, "
				+ "NAME, SHORT_DESCRIPTION, FULL_DESCRIPTION, MAX_PERIOD, MAX_MONEY_AMOUNT, PERCENT, MIN_MONEY_AMOUNT, STATUS) "
        		+ "values (uamaxua.increment_for_credit_program.nextval, ?, ?, ?, ?, ?, ?, ?, ?)" );
        System.out.println("preparedStatement created");
        preparedStatement.setString( 1, entity.getName() );
        preparedStatement.setString( 2, entity.getShortDescription() );
        preparedStatement.setString( 3, entity.getFullDescription() );
        preparedStatement.setInt( 4, entity.getMaxPeriod());
        preparedStatement.setInt( 5, entity.getMaxMoneyAmount() );
        preparedStatement.setFloat( 6, entity.getPercent() );
        preparedStatement.setInt( 7, entity.getMinMoneyAmoun() );
        preparedStatement.setString( 8, entity.getStatus() );
     
        preparedStatement.executeUpdate();
		
		
	}

	public void updateEntity(CreditProgram entity) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "UPDATE uamaxua.BANK_CREDIT_PROGRAM SET "
        		+ "NAME = ?, SHORT_DESCRIPTION = ?, FULL_DESCRIPTION = ?, MAX_PERIOD =?, MAX_MONEY_AMOUNT= ?, PERCENT = ?, MIN_MONEY_AMOUNT = ?,"
        		+ "STATUS = ?  WHERE ID_CREDIT_PROGRAM = ?" );
        preparedStatement.setString( 1, entity.getName() );
        preparedStatement.setString( 2, entity.getShortDescription() );
        preparedStatement.setString( 3, entity.getFullDescription());
        preparedStatement.setInt( 4, entity.getMaxPeriod() );
        preparedStatement.setInt( 5, entity.getMaxMoneyAmount() );
        preparedStatement.setFloat( 6, entity.getPercent() );
        preparedStatement.setInt( 7, entity.getMinMoneyAmoun() );
        preparedStatement.setString( 8, entity.getStatus() );
        preparedStatement.setInt( 9, entity.getIdCreditProgram() );
        preparedStatement.executeUpdate();
	}

	public CreditProgram readEntity(int id, String login) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_credit_program  WHERE ID_CREDIT_PROGRAM = ?" );
        preparedStatement.setInt( 1, id );
        ResultSet resultSet = preparedStatement.executeQuery();
		

        CreditProgram creditProgram = null;
        if ( resultSet.next() ) {
        	creditProgram = new CreditProgram(resultSet.getInt( "ID_CREDIT_PROGRAM" ), resultSet.getString( "NAME" ), 
        			resultSet.getString( "SHORT_DESCRIPTION" ), resultSet.getString( "FULL_DESCRIPTION" ),
        			resultSet.getInt( "MAX_PERIOD" ), resultSet.getInt( "MAX_MONEY_AMOUNT" ), resultSet.getFloat("PERCENT"),
        			resultSet.getInt( "MIN_MONEY_AMOUNT" ), resultSet.getString( "STATUS" )
        			);
        }
        resultSet.close();
        connection.close();
        return creditProgram;
	}

	public void deleteEntity(int id) {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<CreditProgram> readAll() throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM uamaxua.bank_credit_program ORDER BY ID_CREDIT_PROGRAM");
        ArrayList<CreditProgram> creditPrograms = new ArrayList<CreditProgram>();
        CreditProgram temp = null;
        while ( resultSet.next() ) {
        	temp = new CreditProgram(resultSet.getInt( "ID_CREDIT_PROGRAM" ), resultSet.getString( "NAME" ), 
        			resultSet.getString( "SHORT_DESCRIPTION" ), resultSet.getString( "FULL_DESCRIPTION" ),
        			resultSet.getInt( "MAX_PERIOD" ), resultSet.getInt( "MAX_MONEY_AMOUNT" ), resultSet.getFloat("PERCENT"),
        			resultSet.getInt( "MIN_MONEY_AMOUNT" ), resultSet.getString( "STATUS" ));
        	creditPrograms.add(temp);
        }
        resultSet.close();
        connection.close();
        return creditPrograms;
	}

	

}
