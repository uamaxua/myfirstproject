﻿package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vo.CreditProgram;
import vo.Order;

public class DaoOrder implements Dao<Order> {

	public void creatEntity(Order entity) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
		System.out.println("OracleConnect.getOracleConnect()");
        PreparedStatement preparedStatement = connection.prepareStatement( "insert into uamaxua.bank_order (ID_ORDER, ID_CREDIT_PROGRAM, STATUS, LOGIN_MANAGER, DATE_CREATION, DURATION_CREDIT, SUM_CREDIT, LOGIN_CLIENT) "
        		+ "values (uamaxua.INCREMENT_FOR_ORDER.nextval, ?, ?, ?, sysdate, ?, ?, ?)" );
        System.out.println("preparedStatement created");
        preparedStatement.setInt( 1, entity.getIdCreditProgram() );
        preparedStatement.setString( 2, entity.getStatus() );
        preparedStatement.setString( 3, entity.getLoginManager() );
        preparedStatement.setInt( 4, entity.getDurationCredit() );
        preparedStatement.setInt( 5, entity.getSumCredit() );
        preparedStatement.setString( 6, entity.getLoginClient() );
     
        	preparedStatement.executeUpdate();
        
		
	}

	public void updateEntity(Order entity) throws SQLException {
		System.out.println("update Order");
		Connection connection = OracleConnect.getOracleConnect();
		System.out.println("OracleConnect.getOracleConnect()");
        PreparedStatement preparedStatement = connection.prepareStatement( "UPDATE uamaxua.bank_order SET "
        		+ "LOGIN_MANAGER = ?, STATUS = ? WHERE ID_ORDER = ?" );
        System.out.println("preparedStatement created");
        preparedStatement.setString( 1, entity.getLoginManager() );
        preparedStatement.setString( 2, entity.getStatus() );
        preparedStatement.setInt( 3, entity.getIdOrder() );
       
     
        	preparedStatement.executeUpdate();
		
	}

	public Order readEntity(int id, String login) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_order  WHERE ID_ORDER = ?" );
        preparedStatement.setInt( 1, id );
        ResultSet resultSet = preparedStatement.executeQuery();
		
        Order order = null;
        if ( resultSet.next() ) {
        	order= new Order(resultSet.getInt( "ID_ORDER" ), resultSet.getInt( "ID_CREDIT_PROGRAM" ), resultSet.getString( "STATUS" ),
        			resultSet.getString( "LOGIN_MANAGER" ), resultSet.getString( "DATE_CREATION" ), resultSet.getInt( "DURATION_CREDIT" ),
        			resultSet.getInt( "SUM_CREDIT" ), resultSet.getString( "LOGIN_CLIENT" ) );
        }
        resultSet.close();
        connection.close();
        return order;
	}

	public void deleteEntity(int id) {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<Order> readAllByLogin(String login) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_order  WHERE LOGIN_CLIENT = ?" );
        preparedStatement.setString( 1, login );
        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<Order> orders = new ArrayList<Order>();
        Order temp = null;
        while ( resultSet.next() ) {
        	temp= new Order(resultSet.getInt( "ID_ORDER" ), resultSet.getInt( "ID_CREDIT_PROGRAM" ), resultSet.getString( "STATUS" ),
        			resultSet.getString( "LOGIN_MANAGER" ), resultSet.getString( "DATE_CREATION" ), resultSet.getInt( "DURATION_CREDIT" ),
        			resultSet.getInt( "SUM_CREDIT" ), resultSet.getString( "LOGIN_CLIENT" ) );
        			orders.add(temp);
         }
        resultSet.close();
        connection.close();
        return orders;
	}
	
	public ArrayList<Order> readAllByLoginManager(String login) throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_order  WHERE LOGIN_MANAGER = ? AND status = 'В огляді'" );
        preparedStatement.setString( 1, login );
       
        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<Order> orders = new ArrayList<Order>();
        Order temp = null;
        while ( resultSet.next() ) {
        	temp= new Order(resultSet.getInt( "ID_ORDER" ), resultSet.getInt( "ID_CREDIT_PROGRAM" ), resultSet.getString( "STATUS" ),
        			resultSet.getString( "LOGIN_MANAGER" ), resultSet.getString( "DATE_CREATION" ), resultSet.getInt( "DURATION_CREDIT" ),
        			resultSet.getInt( "SUM_CREDIT" ), resultSet.getString( "LOGIN_CLIENT" ) );
        			orders.add(temp);
         }
        resultSet.close();
        connection.close();
        return orders;
	}
	

	public ArrayList<Order> readAll() throws SQLException {
		Connection connection = OracleConnect.getOracleConnect();
        PreparedStatement preparedStatement = connection.prepareStatement( "SELECT * FROM uamaxua.bank_order WHERE status = 'В черзі'" );
       
        ResultSet resultSet = preparedStatement.executeQuery();
        ArrayList<Order> orders = new ArrayList<Order>();
        Order temp = null;
        while ( resultSet.next() ) {
        	temp= new Order(resultSet.getInt( "ID_ORDER" ), resultSet.getInt( "ID_CREDIT_PROGRAM" ), resultSet.getString( "STATUS" ),
        			resultSet.getString( "LOGIN_MANAGER" ), resultSet.getString( "DATE_CREATION" ), resultSet.getInt( "DURATION_CREDIT" ),
        			resultSet.getInt( "SUM_CREDIT" ), resultSet.getString( "LOGIN_CLIENT" ) );
        			orders.add(temp);
         }
        resultSet.close();
        connection.close();
        return orders;
	}	
	
	
	}

