﻿package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;



public class ServerLauncher {
	private final static int SERVER_PORT = 3555;
	
	public static void main(String[] args) throws  IOException {
	
		final ServerSocket serverSocket = new ServerSocket(SERVER_PORT);
		System.out.println("Server: Server started.");
		System.out.println("Server: Server is working at port "+SERVER_PORT);
		System.out.println();
		while(true){
			Socket socket = serverSocket.accept();
			
			new Thread(new SocketThread(socket)).start();
		}
			

	}

}