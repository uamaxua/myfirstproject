﻿package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import vo.*;
import dao.*;

public class SocketThread implements Runnable {
	private Socket socket;
	private String input;
	private ObjectInputStream inStream;
	private ObjectOutputStream outStream;
	
	private SocketThread() {
		}

	public SocketThread(Socket socket) {
		this.socket = socket;	
	}

	public void run() {
		
		
		System.out.println("Server:" + "New SocketThred was created.");			
			try {
			
			inStream = new ObjectInputStream(socket.getInputStream());
			
			outStream = new ObjectOutputStream(socket.getOutputStream());
			input = (String) inStream.readObject();
			
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} 
			
	//**************************************************************************************************			
				if(input!=null){
						System.out.println("input="+input);
	//****************************************************************************************************							
				if("password_identity".equals(input) ){
				  try {
					  
					String login=(String) inStream.readObject();
					String password=(String) inStream.readObject();
					DaoUser daoUser = new DaoUser();
					User user = daoUser.readEntity(0, login);
					if(password != null && user != null && password.equals(user.getPassword())
							&& !("blocked".equals(user.getStatus()))  ){ 
							outStream.writeObject("allow");
							outStream.writeObject(user.getRole());
							outStream.writeObject(user.getLastName()+" "+user.getFirstName()+" "+user.getMiddleName());
						}
						
						else { 	outStream.writeObject("deny"); 	}
					} catch (IOException e) {
						e.printStackTrace();
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						
						e.printStackTrace();
					}						
				}
	//***********************************************************************************			
				if("credit_program_get".equals(input)){
					
					try {
						outStream.writeObject((new DaoCreditProgram()).readAll());
						
					} catch (SocketException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}	
	//***********************************************************************************************				
		if("user_client_get".equals(input)){
			
					try {
						String login= (String) inStream.readObject();
						System.out.println("Login"+login);
						User user = new DaoUser().readEntity(0, login);
						ArrayList<Order> orders = new DaoOrder().readAllByLogin(login);
						outStream.writeObject(user);
						outStream = new ObjectOutputStream(socket.getOutputStream());
						outStream.writeObject(orders);
						Map<String, User> usersNoPassword = new DaoUser().safeReadAll();
						outStream = new ObjectOutputStream(socket.getOutputStream());
						outStream.writeObject(usersNoPassword);
						outStream = new ObjectOutputStream(socket.getOutputStream());
						DaoCreditUnit daoCreditUnit = new DaoCreditUnit();
						ArrayList<CreditUnit> creditUnits = daoCreditUnit.readEntityByLogin(0, login);
						outStream.writeObject(creditUnits);
						
					} catch (SocketException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (SQLException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}	
	//***********************************************************************************************

		if("user_order_write".equals(input)){
			DaoOrder daoOrder = new DaoOrder();
			Order order = null;
				try {
					inStream = new ObjectInputStream(socket.getInputStream());
					order = (Order) (inStream.readObject());
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			try {
				daoOrder.creatEntity(order);
				try {
					outStream.writeObject("yes");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				try {
					outStream.writeObject("no");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		
		
//***********************************************************************************************				
		if("update_user".equals(input)){
				try {
					inStream = new ObjectInputStream(socket.getInputStream());
					
					User oldUser = null;
					User updatedUser = null;
					updatedUser = (User) (inStream.readObject());
					DaoUser daoUser = new DaoUser();
					oldUser = daoUser.readEntity(0, updatedUser.getLogin());
					
					if (updatedUser.getPassword().equals(oldUser.getPassword())){
						
						daoUser.updateEntity(updatedUser);
						outStream.writeObject("yes");
					}
						
					else {
						
						outStream.writeObject("no");
						}
					
					
							} catch (SocketException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}	

		
		
//***********************************************************************************************
		if("create_program".equals(input)){
			System.out.println("Started create credit prog");	
			try {
				inStream = new ObjectInputStream(socket.getInputStream());
				
				CreditProgram newCreditProgram = null;
				newCreditProgram = (CreditProgram) (inStream.readObject());
				DaoCreditProgram daoCreditProgram = new DaoCreditProgram();
				
				
				daoCreditProgram.creatEntity(newCreditProgram);
				outStream.writeObject("yes");
				
				
				
						} catch (SocketException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (ClassNotFoundException e) {
							
							e.printStackTrace();
						} catch (SQLException e) {
							
							e.printStackTrace();
						}
					}	

	
//***********************************************************************************************
				if("update_program".equals(input)){
					System.out.println("Started create credit prog");	
					try {
						inStream = new ObjectInputStream(socket.getInputStream());
						
						CreditProgram newCreditProgram = null;
						newCreditProgram = (CreditProgram) (inStream.readObject());
						System.out.println(newCreditProgram.getName());
						System.out.println(newCreditProgram.getShortDescription());
						System.out.println("IDDDDDD="+newCreditProgram.getIdCreditProgram());
						DaoCreditProgram daoCreditProgram = new DaoCreditProgram();
						
						daoCreditProgram.updateEntity(newCreditProgram);
						outStream.writeObject("yes");
						
						
						
								} catch (SocketException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								} catch (ClassNotFoundException e) {
									
									e.printStackTrace();
								} catch (SQLException e) {
									
									e.printStackTrace();
								}
							}	

//***********************************************************************************************				
				if("find_user".equals(input)){
					
							try {
								System.out.println("find started");
								String login = (String) inStream.readObject();
								String firstName = (String) inStream.readObject();
								String lastName = (String) inStream.readObject();
								System.out.println("Login"+login);
								
								ObjectOutputStream outStreamObj = new ObjectOutputStream(socket.getOutputStream());
								DaoUser daoUser = new DaoUser();
								ArrayList<User> users = daoUser.findEntity(login, firstName, lastName);
								outStreamObj.writeObject(users);
								outStream.writeObject("yes");
								
								
							} catch (SocketException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}				

				
				
//***********************************************************************************************
							
				if("find_user_manager".equals(input)){
					
							try {
								System.out.println("find started");
								String login = (String) inStream.readObject();
								String firstName = (String) inStream.readObject();
								String lastName = (String) inStream.readObject();
								System.out.println("Login"+login);
								
								ObjectOutputStream outStreamObj = new ObjectOutputStream(socket.getOutputStream());
								DaoUser daoUser = new DaoUser();
								ArrayList<User> users = daoUser.findEntityManager(login, firstName, lastName);
								outStreamObj.writeObject(users);
								outStream.writeObject("yes");
								
								
							} catch (SocketException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}				

				
				
//***********************************************************************************************
				

				
				if("find_credit_unit".equals(input)){
					
							try {
								System.out.println("find started");
								String login = (String) inStream.readObject();
								System.out.println("Login"+login);
								
								ObjectOutputStream outStreamObj = new ObjectOutputStream(socket.getOutputStream());
								DaoCreditUnit daoCreditUnit = new DaoCreditUnit();
								ArrayList<CreditUnit> creditUnits = daoCreditUnit.findEntityLogin(login);
								outStreamObj.writeObject(creditUnits);
								outStream.writeObject("yes");
								
								
							} catch (SocketException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}				

				
				
//***********************************************************************************************				
				
				
				if("update_user_admin".equals(input)){
						try {
							inStream = new ObjectInputStream(socket.getInputStream());
							
							User updatedUser = null;
							updatedUser = (User) (inStream.readObject());
							DaoUser daoUser = new DaoUser();
							daoUser.updateEntity(updatedUser);
							outStream.writeObject("yes");
							
							
									} catch (SocketException e) {
										e.printStackTrace();
									} catch (IOException e) {
										e.printStackTrace();
									} catch (ClassNotFoundException e) {
										e.printStackTrace();
									} catch (SQLException e) {
										e.printStackTrace();
									}
								}	
//***********************************************************************************************				
				if("create_user_admin".equals(input)){
						try {
							inStream = new ObjectInputStream(socket.getInputStream());
			
							User newUser = null;
							newUser = (User) (inStream.readObject());
							DaoUser daoUser = new DaoUser();
							daoUser.creatEntity(newUser);;
						
							daoUser.updateEntity(newUser);
							outStream.writeObject("yes");
							
					
									} catch (SocketException e) {
										e.printStackTrace();
									} catch (IOException e) {
										e.printStackTrace();
									} catch (ClassNotFoundException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (SQLException e) {
										try {
											outStream.writeObject("no");
										} catch (IOException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
										e.printStackTrace();
									}
								}	
//***********************************************************************************************				
				if("orders_update".equals(input)){
					
							try {
								inStream = new ObjectInputStream(socket.getInputStream());
								Order newOrder = (Order) inStream.readObject();
								DaoOrder daoOrder = new DaoOrder();
								daoOrder.updateEntity(newOrder);
								outStream.writeObject("yes");							
								
								
								
							} catch (SocketException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (SQLException e) {
								e.printStackTrace();
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}	
			

//***********************************************************************************************
							
				if("update_credit_unit".equals(input)){
					
							try {
								inStream = new ObjectInputStream(socket.getInputStream());
								CreditUnit newCreditUnit = (CreditUnit) inStream.readObject();
								DaoCreditUnit daoCreditUnit = new DaoCreditUnit();
								daoCreditUnit.updateEntity(newCreditUnit);
								outStream.writeObject("yes");							
								
								
								
							} catch (SocketException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (SQLException e) {
								e.printStackTrace();
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							}
						}	
			

//***********************************************************************************************				
				if("credit_unit_create".equals(input)){
					
							try {
								inStream = new ObjectInputStream(socket.getInputStream());
								Order newOrder = (Order) inStream.readObject();
								inStream = new ObjectInputStream(socket.getInputStream());
								CreditUnit newCredit = (CreditUnit) inStream.readObject();
								DaoOrder daoOrder = new DaoOrder();
								daoOrder.updateEntity(newOrder);
								DaoCreditUnit daoCreditUnit = new DaoCreditUnit();
								daoCreditUnit.creatEntity(newCredit);
								
								outStream.writeObject("yes");							
								
								
								
							} catch (SocketException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (SQLException e) {
								e.printStackTrace();
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}	
			

//***********************************************************************************************				
	
				if("orders_get".equals(input)){
					
							try {
								
								String login = (String) inStream.readObject();
								
								ArrayList<Order> orders = new DaoOrder().readAll();
								ArrayList<Order> orders1 = new DaoOrder().readAllByLoginManager(login);
								
								outStream.writeObject(orders);
								outStream.writeObject(orders1);
								
							} catch (SocketException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							} catch (SQLException e) {
								e.printStackTrace();
							} catch (ClassNotFoundException e) {
								e.printStackTrace();
							}
						}	
			

//***********************************************************************************************				

	}
//***********************************************************************************************					
		try {
			socket.close();
			System.out.println("Socket was closed");
			System.out.println();
		} catch (IOException e) {
			e.printStackTrace();
		}			
	
	}
}
