﻿package vo;

import java.io.Serializable;

public class CreditUnit implements Serializable {

	private int idCredit;
	private int idOrder;
	private String dateStart;
	private String dateEnd;
	private String status;
	private String login;

	private CreditUnit(){
		
	}

	public CreditUnit(int idCredit, int idOrder, String dateStart,
			String dateEnd, String status, String login) {
		this.idCredit = idCredit;
		this.idOrder = idOrder;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.status = status;
		this.login = login;
	}

	public int getIdCredit() {
		return idCredit;
	}

	public void setIdCredit(int idCredit) {
		this.idCredit = idCredit;
	}

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idCreditProgram) {
		this.idOrder = idOrder;
	}

	public String getDateStart() {
		return dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public String getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	

}
