# README #

ПРАКТИЧЕСКИЙ ПРОЕКТ - ЗАДАНИЕ

Идея – информационная поддержка системы кредитования.
Имеется некоторое количество кредитных программ, по которым можно  оформлять кредиты. Пользователи должны иметь возможность просматривать информацию о кредитных программах.
Потенциальные клиенты должны иметь возможность регистрироваться в системе. О каждом клиенте должны храниться фамилия, имя, отчество, уровень дохода и наличие поручительства.
Зарегистрированные клиенты должны иметь возможность изменять свои регистрационные данные, просматривать свою кредитную историю, а также делать запросы на получение кредита по определенной программе.
Кредитный менеджер должен иметь возможность просматривать заявки на кредиты, просматривать данные о заявителе, включая его кредитную историю, и на основании этого принимать решение о предоставлении кредита или отказе в кредите.
Администратор должен иметь возможность изменять данные о кредитных программах.

Приложение должно быть построено на основе клиент-серверной архитектуры с использованием сокетов. Данные должны храниться в реляционных СУБД. Уровень бизнес-логики должен быть построен на основе модели предметной области, т.е. спроектированные классы должны адекватно моделировать предметную область. Для этого использовать концепцию DAO и VO.
	
Должна обеспечиваться корректная работа с клиентами в условиях многопоточности.

